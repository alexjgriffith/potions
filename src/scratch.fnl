(local map (require :lib.editor.map))
(local state (require :state))
(local fennel (require :fennel))

(fennel.view (map.remove-brushes (map.serialize state.level))
             {:line-length 300

              :prefer-colon? true})
