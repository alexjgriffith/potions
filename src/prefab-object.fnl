(fn wrap-grass-shader [self draw-shaded draw-unshaded?] ; depricate
  (local lg love.graphics)
  (local grass-shader (love.graphics.getShader))
  (when (and grass-shader (grass-shader:hasUniform :spriteSize))
    (local (px py pw ph) (self.quad:getViewport))
    (grass-shader:send :spriteSize [pw ph])
    (grass-shader:send :spriteAtlasPosition [px py])
    (grass-shader:send :depth 4)
    ;; (+ self.y self.h)
    ;;(+ self.y self.h)
    ;; (grass-shader:send :below (if (<= self.h 16) 1 0))
    (grass-shader:send :highlight (or self.highlight 0)))
  (draw-shaded self)
  (when (and grass-shader (grass-shader:hasUniform :spriteSize))
    (grass-shader:send :highlight 0))
  (set self.highlight 0)
  (when draw-unshaded?
    (lg.setShader)
    (draw-unshaded? self)
    (lg.setShader grass-shader)))

(fn grass-shader-pipeline [self callback dx? dy?]
  (let [lg love.graphics
        sp (require :sprite-pipeline)
        (px py pw ph) (self.quad:getViewport)
        {: grass-shader : cliff-shader : flip-shader} (require :shaders)
        canvas (lg.getCanvas)
        dx (or dx? 0)
        dy (or dy? 0)
        ox self.canvas-offset.x
        oy self.canvas-offset.y]
    ;; (pp [self.name self.sprite-pipeline-index])
    (-> callback
        (sp.start self.sprite-pipeline-index ox oy)
        (sp.shader-pass grass-shader
                        {:spriteSize [pw ph]
                         :depth 4
                         :offset [ox oy]
                         :pos [self.x self.y]})
        (sp.shader-pass cliff-shader
                        {:spriteSize [pw ph]
                         :pos [(- self.x ox) (- self.y oy)]
                         :above 0.0})
        (sp.end canvas (- self.x dx) (- self.y dy ) ox oy))))

(fn water-flip-pipeline [self callback dx? dy?]
  (let [lg love.graphics
        sp (require :sprite-pipeline)
        (px py pw ph) (self.quad:getViewport)
        {: grass-shader : cliff-shader : flip-shader : reflection-shader} (require :shaders)
        canvas (lg.getCanvas)
        dx (or dx? 0)
        dy (or dy? 0)
        ox self.canvas-offset.x
        oy self.canvas-offset.y]
    (-> callback
        (sp.start self.sprite-pipeline-index ox oy)
        (sp.shader-pass grass-shader
                        {:spriteSize [pw ph]
                         :depth 4
                         :offset [ox oy]
                         :pos [self.x self.y]})
        (sp.shader-pass cliff-shader
                        {:spriteSize [pw ph]
                         :pos [(- self.x ox) (- self.y oy)]
                         :above 0.0})
        (sp.shader-pass flip-shader
                        {:spriteSize [pw ph]
                         :oh self.canvas-offset.oh
                         :oy self.canvas-offset.oy
                         :offset [ox oy]})
        (sp.end canvas (- self.x dx) (- self.y dy (- self.h)) ox oy  reflection-shader))))


(fn draw-callback [self image]
    (fn [] (love.graphics.draw image self.quad)))

(local object
       {:serialize
        (fn [self]  (print (.. "Encoding " self.name)) self.encode)
        : wrap-grass-shader
        : grass-shader-pipeline
        : water-flip-pipeline
        :draw (fn [self image ...] (grass-shader-pipeline self (draw-callback self image)))
        :draw-water (fn [self image ...] (water-flip-pipeline self (draw-callback self image)))

        ;;(wrap-grass-shader self (fn [] (love.graphics.draw image self.quad self.x self.y)))
               :draw-shadow
               (fn [self image ...]
                 (local lg love.graphics)
                 (when (and self.shadow self.shadow-quad)
                   (local grass-shadow (lg.getShader))
                   (if (and self.highlight (= self.highlight 1))
                       (do (set self.highlight 0)
                           (love.graphics.setColor 1 1 0 0.2))
                       (love.graphics.setColor 0 0 0 0.2))
                   (when grass-shadow (grass-shadow:send :onField false))
                   
                   (love.graphics.draw image self.shadow-quad (+ (or self.sx 0) self.x) (+ self.y (or self.sy 0)))
                   (when grass-shadow (grass-shadow:send :onField true)
                         (love.graphics.draw image self.shadow-quad (+ (or self.sx 0) self.x) (+ self.y (+ -2 (or self.sy 0)))))
                   ))
               :draw-outline
               (fn [self image ...]
                 (when (and self.outline self.outline-quad)
                   (local lg love.graphics)
                   
                   (lg.setColor 1 1 1 0.05)
                   (local quad self.outline-quad)
                   (local grass-shader (lg.getShader))
                   (local (px py pw ph) (quad:getViewport))
                   (grass-shader:send :spriteSize [pw ph])
                   (grass-shader:send :spriteAtlasPosition [px py])
                   (lg.draw image quad self.x self.y)))
               :set-highlight (fn [self] (set self.highlight 1))
               :update
               (fn [self dt])
               :handlers {}})


(local object-mt {:__index object})

(fn object.remove [obj]
  (local {: level} (require :state))
  (level:remove-obj :objects obj)
  (let [{: world} (require :state)]
    (when (world:hasItem obj)
      (world:remove obj))))

(fn object.re-add [obj]
  (local {: level} (require :state))
  (level:add :objects obj.x obj.y obj.name))


(fn object.remove-object [obj]
  (let [{: world} (require :state)]
    (when (world:hasItem obj)
        (world:remove obj)
        )))

(fn object.add-sprite-pipeline-canvases [self]
  (let [sprite-pipeline (require :sprite-pipeline)]
    (assert self.w (.. "Object " self.name "is missing w."))
    (assert self.h (.. "Object " self.name "is missing h."))
    ;; (pp [self.name self.w self.h])
    ;; (or (and self.canvas-offset self.canvas-offset.ox) 0)
    ;; (or (and self.canvas-offset self.canvas-offset.oy) 0)
    (when (not self.canvas-offset)
      (tset self :canvas-offset {:x 16 :y 16 :w 32 :h 32
                                 :oh (or self.encode.oh (+ self.encode.h 32))
                                 :oy (or self.encode.oy 0)}))
    (tset self :sprite-pipeline-index (sprite-pipeline.create-index
                                       (+ self.w self.canvas-offset.w)
                                       (+ self.h self.canvas-offset.h))))
  self)

(fn object.add-collider [obj ?type]
  (let [{: world} (require :state)        
        {: cx : cy : cw : ch} obj.encode]
    (when (and cx cy cw ch)
      (tset obj :collider (or ?type :slide))
      (world:add obj (+ obj.x cx) (+ obj.y cy) cw ch))
    obj))

(fn object.draw-collider [obj ?scale]
  (let [{: world} (require :state)
        lg love.graphics
        scale (or ?scale 1)
        (cx cy cw ch) (if (world:hasItem obj) (world:getRect obj) (values nil nil nil nil))]
    (when (and cx cy cw ch)
      (lg.push :all)
      (lg.setColor 1 1 0 0.5)
      (lg.rectangle :line (* scale cx) (* scale cy) (* scale cw) (* scale ch))
      (lg.pop)
    obj)
  ))

(fn object.preview [brush]
  (local {: px : py : pw : ph : iw : ih : name} brush)
  (local obj (-> {:quad (love.graphics.newQuad px py pw ph iw ih)
                  : name
                  :encode brush
                  :w brush.w :h brush.h
                  :x brush.x :y brush.y}
                 ((fn [x] (setmetatable x object-mt) x))
                 object.add-sprite-pipeline-canvases
      ))
  obj)


(fn object.create [brush]
  (local {: px : py : pw : ph : iw : ih : name} brush)
  (local {: atlas} (require :assets))
  (var type nil)
  (match name
    :SmallStone (set type :stone)
    :Stump (set type :stump))
  (local obj (-> {:encode brush
                  :shadow brush.shadow
                  :shadow-quad (?. atlas.quads brush.shadow)
                  :sy brush.sy
                  :outline brush.shadow
                  :outline-quad (?. atlas.quads brush.outline)
                  :name name
                  :highlight 0
                  : type
                  :quad (love.graphics.newQuad px py pw ph iw ih)
                  :x brush.x :y brush.y
                  :w brush.w :h brush.h}
      ((fn [x] (setmetatable x object-mt) x))
      object.add-collider
      object.add-sprite-pipeline-canvases
      ))
  obj
  )

object
