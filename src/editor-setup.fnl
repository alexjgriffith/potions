(local {: editor : brush} (require :lib.editor.interface))
(local level (require :lib.editor.level))

(tset brush :brushes  [[:ground :ground :tile]
                       [:ground :wheat :tile]
                       [:ground :dirt :tile]
                       [:ground :water :tile]
                       [:ground :cliff :tile]
                       [:objects :Player :object]
                       [:objects :Wizard :object]
                       [:objects :Cat :object]
                       [:objects :Fence :object]
                       [:objects :FenceLeft :object]
                       [:objects :FenceRight :object]
                       [:objects :FenceCenter :object]
                       [:objects :House :object]
                       [:objects :Stump :object]
                       [:objects :Stump2 :object]
                       [:objects :SmallTree :object]
                       [:objects :BigTree :object]
                       [:objects :Pot :object]
                       [:buttons :GreenButton :object]
                       [:objects :GreenFence :object]
                       [:buttons :BlueButton :object]
                       [:objects :BlueFence :object]
                       [:buttons :RedButton :object]
                       [:objects :RedFence :object]
                       [:buttons :YellowButton :object]
                       [:objects :YellowFence :object]
                       [:objects :SmallStone :object]
                       [:objects :BigStone :object]
                       [:objects :Fire1 :object]
                       [:objects :Death :object]
                       [:objects :Life :object]
                       [:objects :Feather :object]
                       [:objects :Carrot :object]
                       [:objects :Seed :object]
                       [:objects :Nut :object]
                       [:objects :Rosemary :object]
                       [:objects :Garlic :object]
                       [:objects :Lemon :object]
                       [:objects :Lilly :object]
                       [:objects :Sunflower :object]
                       [:objects :Dragon :object]
                       [:objects :Hemp :object]
                       [:objects :Lavender :object]
                       [:objects :Root :object]
                       [:objects :Cherry :object]
                       [:buttons :ConUp :object]
                       [:buttons :ConDown :object]
                       [:buttons :ConLeft :object]
                       [:buttons :ConRight :object]
                       [:buttons :Ford :object]
                       ;; [:objects :obj :object]
                       ])

(tset brush :count (# brush.brushes))

(tset brush :index 6)

(local ground-brushes
       {:ground {:name :ground
                :bitmap :bitmap-1
                :bitmap-w 1
                :char :0
                :ix 0
                :iy 8}
        :wheat {:name :wheat
                :bitmap :bitmap-2x2
                :bitmap-w 4
                :char :1
                :ix 4
                :iy 0}
        :dirt   {:name :dirt
                :bitmap :bitmap-2x2
                :bitmap-w 4
                :char :2
                :ix 4
                :iy 4}
        :water {:name :water
                :bitmap :bitmap-2x2
                :bitmap-w 4
                :char :3
                :ix 0
                :iy 4}
        :cliff {:name :cliff
                :bitmap :bitmap-2x2
                :bitmap-w 4
                :char :4
                :ix 4
                :iy 8}
        })


(local player (require :prefab-player))
(local wizard (require :prefab-wizard))
(local cat (require :prefab-cat))
(local pot (require :prefab-pot))
(local fence (require :prefab-fence))
(local ford (require :prefab-ford))
(local button (require :prefab-button))
(local object (require :prefab-object))
(local ingredients (require :prefab-ingredients))

(local creation-callbacks
       {:Player player.create
        :Wizard wizard.create
        :Cat cat.create
        :Fence object.create
        :FenceLeft object.create
        :FenceRight object.create
        :FenceCenter object.create
        :House object.create
        :Stump object.create
        :Stump2 object.create
        :SmallStone object.create
        :BigStone object.create
        :Fire1 object.create
        :SmallTree object.create
        :BigTree object.create
        :Pot pot.create
        :ConUp object.create
        :ConDown object.create
        :ConLeft object.create
        :ConRight object.create
        :Ford ford.create
        :GreenButton button.create
        :GreenFence fence.create
        :BlueButton button.create
        :BlueFence fence.create
        :RedButton button.create
        :RedFence fence.create
        :YellowButton button.create
        :YellowFence fence.create})

(local preview-callbacks
       {:Player player.preview
        :Wizard object.preview
        :Cat object.preview
        :Fence object.preview
        :FenceLeft object.preview
        :FenceRight object.preview
        :FenceCenter object.preview
        :House object.preview
        :Stump object.preview
        :Stump2 object.preview
        :SmallStone object.preview
        :BigStone object.preview
        :Fire1 object.preview
        :SmallTree object.preview
        :BigTree object.preview
        :Pot object.preview
        :ConUp object.preview
        :ConDown object.preview
        :ConLeft object.preview
        :ConRight object.preview
        :Ford ford.preview        
        :GreenButton button.preview
        :GreenFence fence.preview
        :BlueButton button.preview
        :BlueFence fence.preview
        :RedButton button.preview
        :RedFence fence.preview
        :YellowButton button.preview
        :YellowFence fence.preview})


(each [_ ingredient (ipairs ingredients.list)]
  (tset creation-callbacks ingredient ingredients.create))

(each [_ ingredient (ipairs ingredients.list)]
  (tset preview-callbacks ingredient ingredients.preview))

{: editor : brush : level : creation-callbacks : preview-callbacks : ground-brushes}
