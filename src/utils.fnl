(local state (require :state))
(fn update-ref-list [l1]
  (each [i {: name} (ipairs (. (l1.get-layer l1 :objects) :data))]
  (when (not (. state.ref-list name))
    (tset state.ref-list name i))))

(fn get-obj [l1 name]
  (when (. state.ref-list name) (. (l1.get-layer l1 :objects) :data (. state.ref-list name))))

(var debug-time 0)
(local socket (require :socket))
(local first-table {})
(fn first [name count callback ...]
  (when (or (not (. first-table name)) (< (. first-table name) count))
    (tset first-table name (+ (or (. first-table name) 0) 1))
    (callback ...)))

(fn timestep [message]
  (local next-debug-time (* (socket.gettime) 1000))
  (local dt (- next-debug-time debug-time))
  (set debug-time next-debug-time)
  (local garbage (/ (collectgarbage "count") 1000))
  (print (.. dt " - " garbage "KB - " message)))


(fn in [what mx my w h ...]
  (local lg love.graphics)
  (local (screen-mx screen-my) (lg.inverseTransformPoint mx my))
  (local hover (_G.pointWithin screen-mx screen-my 0 0 w h))
  (when hover (love.event.push :hover what ...) true))

(fn structure [t]
  (local structure-type (if (= (type t) :table)
                            (if (. t 1)
                                :array
                                :table)
                            (type t)))
  (fn pt [t]
    (each [key value (pairs t)]
             (print (.. key " -> " (type value) (if (= :table (type value)) "" (.. " " value))))))
  (match structure-type
    :array (do (print :ARRAY)
               (pt (. t 1)))
    :table (do (print :TABLE)
               (pt t))
    _ (print (.. (string.upper (type t)) " " t)))
  :<>)

(fn member-count [t member]
  (local structure-type
         (if (= (type t) :table)
             (if (. t 1)
                 :array
                 :table)
             (type t)))
  (local count {})
  (match structure-type
    :array 
               (each [key value (ipairs t)]
                 (let [m (. value member)]
                   (when m
                     (if (. count m)
                         (tset count m (+ (. count m) 1))
                         (tset count m 1)
                         )))
                 ))
  count)


(fn example [t member value]
  (local structure-type
         (if (= (type t) :table)
             (if (. t 1)
                 :array
                 :table)
             (type t)))
  (var count nil)
  (match structure-type
    :array 
               (each [key index (ipairs t)]
                 (let [m (. index member)]
                   (when (= m value)
                     (set count index)))
                 ))
  count)

(fn structure1 [t]
  (each [key value (pairs (. t 1))]
    (print (.. key " -> " (type value)))))

{: update-ref-list
 : get-obj
 : in
 : first
 : timestep
 : structure
 : structure1
 : member-count
 : example}
