(fn popup [text period?]
  (local message (require :message))
  (message.write text period?)
  ;;(print text)
  )

(fn er [text period?]
  (local message (require :message))
  (message.error text period?)
  ;;(print text)
  )

(var ending false)
(fn love.handlers.game-over [reason]
  (local fade (require :lib.fade))
  (local gamestate (require :lib.gamestate))
  (when (not ending)
    (set ending true)
    ;;(_G.rl)
    (fade.out (fn [] (set ending false)
                (local state (require :state))
                (tset state :player nil)
                (gamestate.switch (require :mode-ending))))))

(fn love.handlers.ability [name active?]
  (local player (require :player-prefab))
  ;;(when (and active? (. player.ability-text name))
  ;; (popup (. player.ability-text name)))
  ;; find sfx
  )

(fn love.handlers.pickup [name])

(fn love.handlers.missing-action [required-potion sound?]
  (local potions (require :prefab-potions))
  (local {: sounds} (require :assets))
  (local sound (. sounds (or sound? :hover)))
  (assert sound (.. (or sound? :hover) " not present."))
  (sound:stop)
  (sound:play)
  (er (.. "Missing required potion: " (. potions.titles required-potion)) 5))

(fn love.handlers.fence-moved [down]
  (local assets (require :assets))
  (if down (do (assets.sounds.bounce:stop)
               (assets.sounds.bounce:play))
      (do (assets.sounds.hover:stop)
          (assets.sounds.hover:play))))

(fn popup-brew [[success? target reason]]
  (local potions (require :prefab-potions))
  (local title (. potions.titles target))
  (local player (require :prefab-player))
  (local assets (require :assets))
  (local special-text (or (. player.ability-text target) ""))
  (if success?
      (do
        (assets.sounds.page:stop)
        (assets.sounds.page:play)
        (popup (.. "Successfully brewed " title " " special-text)))
      (do
        (assets.sounds.hover:stop)
        (assets.sounds.hover:play)
        (er (.. "Failed to brew " title " - " reason)))
      ))

(fn love.handlers.popup [what info]
  (match what
    :Brew (popup-brew info))
  )

(fn love.handlers.hover [what name ...]
  (local state (require :state))
  (local {: sounds} (require :assets))
  (fn handle-potions [what name]
    (local potions (require :prefab-potions))
    (local title (. potions.titles name))
    (local hint (or (. potions.hint name) ""))
    (popup  (.. title "\n" hint))
    (tset state :hover-info {: what : name :target name}))
  (fn handle-ingredients [what name]
    (tset state :hover-info {: what : name :target name})
    (local ingredients (require :prefab-ingredients))
    (local title (. ingredients.titles name))
    (popup  title))
  (fn handle-button [what name target]
    (tset state :hover-info {: what : name : target})
    (local mute state.mute)
    (match name
      :Brew (popup (.. "Click to brew " target))
      :Buy (popup (.. "Click to buy " target))
      :Sell (popup (.. "Click to sell " target))
      :Mute (popup (.. "Click to " (if mute "unmute" "mute")))
      ;; :x (popup (.. "Click to close"))
      ))
  
  (fn handle-play [])
  (set state.hover-count (+ 1 (or 0 state.hover-count)))
  (when (~= state.hover what)
    (sounds.hover:stop)
    (sounds.hover:play)
    (set state.hover what)
    (match what
      :potions (handle-potions what name ...)
      :ingredients (handle-ingredients what name ...)
      :button (handle-button what name ...)
      :play (handle-play)
      :quit :nothing
      :credits :nothing
      :replay :nothing
      )
    ;;(pp [what name])
    )
  )

(fn remove-object-wrapper [_ _ obj]
  (local state (require :state))
  (local world state.world)
  (pp (world:hasItem obj))
  :done)

(local tile-sub-index
       {1 [4]
        2 [2 3]
        3 [1 3 4]
        4 [3 4]
        5 [1 3]
        6 [2 3 4]
        7 [1 2 3 4]
        8 [1 2 4]
        9 [2]
        10 [1 2]
        11 [1 2 3]
        12 [1 4]
        13 []
        14 [3]
        15 [2 4]
        16 [1]
        :NA []
        })


(local prof false)
(fn replace-tile [layer-name]
  (when prof
    (local profile (require :lib.profile))
    (profile.start))
  (local {: level : world} (require :state))
  (local ground (level:get-layer :ground))
  (local data ground.data)
  ;; remove all objects that have a bitmap index
  (local items (world:getItems))
  (var cleared 0)
  (each [_ i (ipairs items)]
    (when i.bitmap-index
      (world:remove i)
      (set cleared (+ cleared 1))))
  (var cliff 0)
  (var water 0)
  ;; (pp (. data 1))
  (each [_ d (ipairs data)]
    (match d.char
      :4 (do ;; cliff
               (world:add d (* 16 (- d.x 1)) (* 16 (- d.y 1)) 16 16)
               (set cliff (+ cliff 1)))
      :3 (do ;; water
           ;; (pp d)
           (each [_ value (ipairs (. tile-sub-index d.bitmap-index))]
             (match value
               1 (world:add {:bitmap-index true}
                            (* 16 (- d.x 1))
                            (* 16 (- d.y 1)) 10 10)
               2 (world:add {:bitmap-index true}
                            (+ (* 16 (- d.x 1)) 6)
                            (* 16 (- d.y 1)) 10 10)
               3 (world:add {:bitmap-index true}
                            (+ (* 16 (- d.x 1)) 6)
                            (+ (* 16 (- d.y 1)) 6) 10 10)
               4 (world:add {:bitmap-index true}
                            (* 16 (- d.x 1))
                            (+ (* 16 (- d.y 1)) 6) 10 10)))
           (set water (+ water 1)))))
  ;; (print (.. "Cleared " cleared ", Cliffs " cliff ", Water " water))
  (when prof
    (local profile (require :lib.profile))
    (profile.stop)
    (print (profile.report 100))
    )
  )

(fn replace-tiles [layer-name data]
  (when prof
    (local profile (require :lib.profile))
    (profile.start))
  (local {: level : world} (require :state))
  ;; (local ground (level:get-layer :ground))
  ;; (local data ground.data)
  ;; (local ground (level:get-layer :ground))
  ;; (local data ground.data)
  ;; remove all objects that have a bitmap index
  (local items (world:getItems))
  (var cleared 0)
  (each [_ i (ipairs items)]
    (when i.bitmap-index
      (world:remove i)
      (set cleared (+ cleared 1))))
  (var cliff 0)
  (var water 0)
  (local water-width 14)
  ;; (pp (. data 1))
  (each [_ d (ipairs data)]
    (match d.char
      :4 (do ;; cliff
               (world:add d (* 16 (- d.x 1)) (* 16 (- d.y 1)) 16 16)
               (set cliff (+ cliff 1)))
      :3 (do ;; water
           ;; (pp d)
           (each [_ value (ipairs (. tile-sub-index d.bitmap-index))]
             (match value
               1 (world:add {:bitmap-index true}
                            (* 16 (- d.x 1))
                            (* 16 (- d.y 1)) water-width water-width)
               2 (world:add {:bitmap-index true}
                            (+ (* 16 (- d.x 1)) (- 16 water-width))
                            (* 16 (- d.y 1)) water-width water-width)
               3 (world:add {:bitmap-index true}
                            (+ (* 16 (- d.x 1)) (- 16 water-width))
                            (+ (* 16 (- d.y 1)) (- 16 water-width)) water-width water-width)
               4 (world:add {:bitmap-index true}
                            (* 16 (- d.x 1))
                            (+ (* 16 (- d.y 1)) (- 16 water-width)) water-width water-width)))
           (set water (+ water 1)))))
  ;; (print (.. "Cleared " cleared ", Cliffs " cliff ", Water " water))
  (when prof
    (local profile (require :lib.profile))
    (profile.stop)
    (print (profile.report 100))
    )
  )


(fn replace-tile-wrapper [layer-name _brush-name _x _y _tilesize reautotile?]
  (when reautotile? (replace-tile layer-name)))

(fn love.handlers.edit-level [key ...]
  ;; (pp [key ...] )
  ;; :remove-object layer-name brush.name obj
  (match key
    :remove-object (remove-object-wrapper ...)
    :replace-tile (replace-tile ...)
    :replace-tiles (replace-tiles ...)
    )
  )

