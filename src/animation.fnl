(fn animation-update [dt animation]
  (when animation.state-change
    (tset animation :timer 0)
    (tset animation :index 1)
    (tset animation :duration (. animation.durations animation.state))
    (tset animation :state-change false))
  (tset animation :timer (+ animation.timer dt))
  (when (> animation.timer animation.duration)
    (tset animation :timer 0)
    (tset animation :index (+ animation.index 1))
    (when (> animation.index (# (. animation.quads animation.state)))
      (tset animation :index 1))))

(fn draw-animation [animation ...]
  (local lg love.graphics)
  (local quad (. animation.quads animation.state animation.index))
  (lg.draw animation.image quad ...))

{: draw-animation : animation-update}
