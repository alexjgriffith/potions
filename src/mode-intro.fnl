(local lg love.graphics)
(local assets (require :assets))

(local fade (require :lib.fade))
(local bump (require :lib.bump))
(local {: update-camera} (require :camera))
(local {: update-ref-list : get-obj} (require :utils))
(local {: editor : brush} (require :editor-setup))

(local state (require :state))

(local (w h) (love.window.getMode))
(local w2 (math.floor (/ w 2)))
(local global-transform {:x 24 :y 0})


(local shaders (require :shaders))
(local grass-shader (love.graphics.newShader :assets/shaders/grass-1.glsl))

(local grass-shadow-shader (love.graphics.newShader
                            :assets/shaders/grass-shadow.glsl))
(tset shaders :grass-shadow-shader grass-shadow-shader)


(tset shaders :grass-shader (love.graphics.newShader :assets/shaders/grass.glsl))
(tset shaders :cliff-shader (love.graphics.newShader :assets/shaders/cliff.glsl))
(tset shaders :flip-shader (love.graphics.newShader :assets/shaders/flip.glsl))
(tset shaders :reflection-shader (love.graphics.newShader :assets/shaders/reflection.glsl))

(fn click [{: name : target : what}]
  (local interface (require :interface))
  (local assets (require :assets))
  (fn click-button [name target]
    (match name
      :x (when (. interface target)
           (assets.sounds.page:stop)
           (assets.sounds.page:play)
           ;; (pp :Close-mixer)
           (let [t (. interface target)]
             (t.close)))
      :Brew (let [(success? target reason) (state.player:brew target)]
              (when success?
                (interface.mixer.close))
              (love.event.push :popup :Brew [success? target reason])
              )
      :Mute (do (set state.mute (not state.mute))
                (local {: sounds} (require :assets))
                (if state.mute
                    (sounds.bgm:setVolume 0)
                    (sounds.bgm:setVolume 0.3)))
      ))
  (fn click-potions [name]
    (local interface (require :interface))
    (local potions (require :prefab-potions))
    (local situation (potions.open name))
    (interface.mixer.close)
    (interface.mixer.open :potions name situation)
    )
  
  (fn click-ingredients []
    (local interface (require :interface))
    (local ingredients (require :prefab-ingredients))
    (interface.mixer.close)
    (interface.mixer.open :ingredients name (ingredients.open name)))
  (match what
    :button (click-button name target)
    :potions (click-potions name)
    :ingredients (click-ingredients name)))

(fn reset-level [level-name]
  (tset state :camera {:x 100 :y 0 :scale 4 :type :game})
  (tset state :mute (if dev true false))
  (tset state :target {:x 0 :y 0 :snap true})
  (tset state :wiggles {:message-y -60})
  (tset state :world (bump.newWorld))
  (tset state :ref-list {})
  (local level (require :level))
  (tset state :level (level.load (or level-name :main-level)))
  ;; (tset _G :reo (fn [] (level:rebrush-encoded-objects )))
  (tset state :ui-visible true)
  (update-ref-list state.level)
  (tset state :player (get-obj state.level :Player)))

(set _G.rl reset-level)

(local field-mask (require :field-mask))

(fn generate-masks []
  (let [state (require :state)]
    (tset state :wheat-canvas (field-mask.make-mask :wheat))
    (tset state :water-canvas (field-mask.make-mask :water))
    (tset state :cliff-canvas (field-mask.make-mask :cliff))))

(set _G.gm (fn []
             (let [state (require :state) utils (require :utils)] (utils.member-count state.level.layers.ground.data :brush))
             (generate-masks)))


(fn enter [gs previous-gs level-name]
  (reset-level level-name)
  (generate-masks)
  (local {: sounds} (require :assets))
  (when (not state.mute) (sounds.bgm:setVolume 0.3))
  (tset state :day 1)
  ;; (local goals (. (require :prefab-potions) :list))
  (tset state :goals [:LoveMedley                      :EssenceOfStrength
                      :CauseOfDeath
                      :MixtureOfLevitation
                      :ElixerOfLife
                      :Cat])
  (editor.init state.level {:x 0 :y 0 :scale 4})
  (fade.in))

(fn init [self])

(fn mousepressed [self x y button]
  (when (not state.ui-visible) (local (t c) (editor.mousepressed x y button)))
  (when state.hover
    (click state.hover-info)))


(fn mousereleased [self x y button]
  (when (not state.ui-visible) (local (t c) (editor.mousereleased x y button)))
  )

(fn draw-windows [scale]
  (lg.push :all)
  (lg.setColor 1 0 0 0.1)
  (lg.translate (* scale global-transform.x) (* scale global-transform.y))
  (local (w h) (values (* 14 scale 16) (* 9 scale 16)))
  (for [i 1 4]
    (for [j 1 4]
      (lg.rectangle :line
                    (+ (* scale 16) (* 15 16 scale (- i 1)))
                    (+ (* scale 16) (* 10 16 scale (- j 1)))  w h)))
  (lg.pop))

(fn draw-grass-mask [mask-name?]
  (let [state (require :state)
        mask (. state (or mask-name? :wheat-canvas))]
    (lg.push)
    (lg.setColor 1 1 1 0.5)
    (lg.draw mask)
    (lg.setColor 1 1 1 1)
    (lg.pop)))

(local (w h) (love.window.getMode))
(local render-pipeline
       {:c1 (lg.newCanvas w h)
        :c2 (lg.newCanvas w h)
        :c3 (lg.newCanvas w h)
        :mask (lg.newCanvas w h)})
(tset shaders :render-pipeline render-pipeline)
(local water-shader (love.graphics.newShader :assets/shaders/water.glsl))
(local simplex-noise (require :lib.simplex-noise-tile-generator))
(local noise-w 128)
(local noise (simplex-noise 1 3 noise-w noise-w))
(var time 0)

(fn draw-game []
  (lg.push :all)
  (local camera state.camera)
  (lg.translate (* camera.scale global-transform.x) (* camera.scale global-transform.y))
  (lg.scale camera.scale)  
  (lg.translate camera.x camera.y)
  
  (lg.setCanvas render-pipeline.mask)
  (lg.clear)
  (lg.setShader)
  (draw-grass-mask :water-canvas)
  
  (lg.setCanvas render-pipeline.c1)
  (lg.clear)
  (state.level:draw-layer :ground)
  (state.level:draw-layer :buttons)

  ;; (lg.setShader reflection-shader)
  ;;  (reflection-shader:send :waterCanvasSize [1280 720])
  (when (shaders.reflection-shader:hasUniform :camera)
    (shaders.reflection-shader:send :camera [(+ global-transform.x camera.x) camera.y]))
  (when (shaders.reflection-shader:hasUniform :mask)
    (shaders.reflection-shader:send :mask render-pipeline.mask))
  (state.level:draw-layer-object-method :objects :draw-water)
  ;; grass shader
  (lg.setShader grass-shadow-shader)
  (grass-shadow-shader:send :camera [(+ global-transform.x camera.x) camera.y])
  (grass-shadow-shader:send :mask state.wheat-canvas)
  (grass-shadow-shader:send :cliffMask state.cliff-canvas)
  (grass-shadow-shader:send :grassCanvasSize [1280 720])
  (grass-shadow-shader:send :scale camera.scale)
  (state.level:draw-layer-object-method :objects :draw-shadow)
  ;; (lg.setShader reflection-shader)
  ;; (reflection-shader:send :camera [(+ global-transform.x camera.x) camera.y])
  ;; (reflection-shader:send :waterCanvasSize [1280 720])
  ;; (reflection-shader:send :scale camera.scale)
  ;; (reflection-shader:send :mask state.water-mask)
  ;; (state.level:draw-layer :objects)
  
  ;; (draw-grass-mask :water-canvas)
  (lg.setCanvas render-pipeline.c2)
  (lg.setColor 1 1 1 1)
  (lg.clear)
  (lg.setShader grass-shader)
  ;; (pp camera)
  (local ({:iw iw :ih ih}) assets.atlas.brushes.Player)
  (when (shaders.grass-shader:hasUniform :camera)
    (shaders.grass-shader:send :camera [(+ global-transform.x camera.x) camera.y]))
  (shaders.grass-shader:send :mask state.wheat-canvas)
  (shaders.grass-shader:send :grassCanvasSize [1280 720])
  (when (shaders.grass-shader:hasUniform :scale)
      (shaders.grass-shader:send :scale camera.scale))

  (when (shaders.cliff-shader:hasUniform :camera)
    (shaders.cliff-shader:send :camera [(+ global-transform.x camera.x) camera.y]))
  (shaders.cliff-shader:send :mask state.cliff-canvas)
  (shaders.cliff-shader:send :grassCanvasSize [1280 720])
  (when (shaders.cliff-shader:hasUniform :scale)
      (shaders.cliff-shader:send :scale camera.scale))

  
  (grass-shader:send :camera [(+ global-transform.x camera.x) camera.y])
  (grass-shader:send :atlasSize [iw ih])
  (grass-shader:send :mask state.wheat-canvas)
  (grass-shader:send :cliffMask state.cliff-canvas)
  (grass-shader:send :grassCanvasSize [1280 720])
  (grass-shader:send :scale camera.scale)
  (state.level:draw-layer :objects)
  (state.level:draw-layer-object-method :objects :draw-outline)
  
  (lg.translate (* camera.scale global-transform.x) (* camera.scale global-transform.y))
  (lg.pop)

  (lg.setCanvas)
  (lg.setShader water-shader)
  (water-shader:send :sprite_width w)
  (water-shader:send :noise_width noise-w)
  (water-shader:send :mask render-pipeline.mask)
  (water-shader:send :noise noise)
  (water-shader:send :time time)
  (lg.draw render-pipeline.c1)
  (lg.setShader)
  (lg.draw render-pipeline.c2)
  ;; (lg.draw render-pipeline.mask)
  )

(fn draw-colliders []
  (local items (state.world:getItems))
  (local camera state.camera) 
  (let [s camera.scale]
    (each [_ item (ipairs items)]
      (local (x y w h) (state.world:getRect item))
      (lg.push :all)
      (lg.setColor 1 1 0 0.5)
      (lg.translate (* camera.scale global-transform.x) (* camera.scale global-transform.y))
      (lg.translate (* camera.scale camera.x) (* camera.scale camera.y))
      (lg.rectangle :line (* x s) (* y s) (* w s) (* h s))
      (lg.pop))))

(fn draw [self message]
  (local interface (require :interface))
  (local camera state.camera) 
  (interface.init-hover)
  (lg.setColor 0 0 0 1)
  (lg.rectangle :fill 0 0 1280 1280)
  (lg.setColor 1 1 1 1)
  (draw-game)

  (when false
    (lg.push :all)
    (lg.reset)
    (lg.setFont assets.font.main-title)
    (local {: shadow} (require :pallet))
    (lg.setColor shadow)
    (lg.scale 4)
    (lg.translate 110 143)
    (lg.print "Potions")
    (lg.pop))
  
  (when  (not state.ui-visible)
    (draw-colliders)
    (draw-windows camera.scale)
    (editor.draw))
  
  (local player state.player)
  (when state.ui-visible
    (lg.push :all)
    (lg.scale camera.scale)
    (lg.draw assets.ui.image (. assets.ui.quads :Background))
    (interface.item-slots.draw player)
    (interface.mixer.draw player)
    (interface.text-box.draw)
    (local message (require :message))
    (message.draw)
    (lg.pop))
  (lg.draw fade.canvas)
  )

(fn add-cameras [a b]
  {:scale a.scale :x (+ a.x b.x) :y (+ a.y b.y)})

(var frame 1)
(fn update [self dt set-mode]
  (set time (+ time dt)) ;; used by shader
  (local interface (require :interface))
  ;; (let [clouds (require :singleton-clouds)] (clouds.update dt))
  (update-camera state.player state.camera state.target)
  (state.level:update-layer :buttons dt)
  (state.level:update-layer :objects dt)
  (state.level:y-sort-layer :objects)
  (when (not state.ui-visible)
    (editor.update dt (add-cameras state.camera global-transform)))
  (set frame (+ frame 1)))

{:draw draw
 : mousepressed
 : mousereleased
 : init
 : enter
 :update update
 :keypressed (fn keypressed [self key code]
               (fn toggle-ui [] (set state.ui-visible (not state.ui-visible)))
               (match key
                 :space (when state.ui-visible (state.player:interact))
                 :tab (toggle-ui)
                 :p (when (not state.ui-visible)
                      (pp (.. "Saving as: " (.. state.level.name ".fnl")))
                      (state.level:save (.. (love.filesystem.getSource) "src/" state.level.name ".fnl")))
                 :1 (when (not state.ui-visible) (tset state.camera :scale 1))
                 :2 (when (not state.ui-visible) (tset state.camera :scale 4))
                 :3 (when (not state.ui-visible) (tset brush :index 30))
                 :4 (when (not state.ui-visible) (tset brush :index 6))
                 :5 (when (not state.ui-visible) (tset brush :index 1))
                 :6 (when (not state.ui-visible) (tset brush :index 19))
                 :7 (when (not state.ui-visible) (tset brush :index 27))
                 :8 (when (not state.ui-visible) (tset brush :index (+ 16 27 3)))
                 :- (do (set state.player.inventory.potions.ElixerOfLife 1)
                        (set state.player.inventory.potions.EssenceOfStrength 1)
                        (set state.player.inventory.potions.LoveMedley 1))
                 :escape (when dev (love.event.quit))
                 ;; :r (when (not state.ui-visible) (level.update-spritebatch state.level :ground))
                 _ (when (not state.ui-visible) (editor.keypressed key code)))
               )}
