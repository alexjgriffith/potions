(local {: add-collider : draw-collider : preview : remove : set-highlight
        : draw-shadow}
       (require :prefab-object))

(local ingredient {:serialize
                   (fn [self]  (print (.. "Encoding " self.name)) self.encode)

                   :draw
                   (fn [self image ...]
                     ;; (love.graphics.draw image self.quad self.x self.y)
                     (local depth (+ 2 (math.abs (- self.offset 4))))
                     (local grass-shader (love.graphics.getShader))
                     (when grass-shader
                       (local (px py pw ph) (self.quad:getViewport))
                       (grass-shader:send :depth (- 4 depth))
                       (grass-shader:send :spriteSize [pw ph])
                       (grass-shader:send :spriteAtlasPosition [px py])
                       (grass-shader:send :highlight self.highlight))
                     (love.graphics.draw image self.quad
                                         self.x (- self.y depth))
                     (grass-shader:send :highlight 0))
                   : draw-shadow
                   ;; (fn [self image ...]
                   ;;   (local lg love.graphics)
                   ;;   ;; (pp self.shadow-quad)
                   ;;   (when  self.shadow-quad
                   ;;     (if (= self.highlight 1)
                   ;;         (love.graphics.setColor 1 1 0 0.2)
                   ;;         (love.graphics.setColor 1 1 1 0.2))
                   ;;     (local grass-shadow (lg.getShader))
                   ;;     (when grass-shadow (grass-shadow:send :onField false))
                   ;;     (love.graphics.draw image self.shadow-quad self.x (+ self.y (or self.sy 0)))
                   ;;     (when grass-shadow
                   ;;       (grass-shadow:send :onField true)
                   ;;       (love.graphics.draw image self.shadow-quad self.x (+ self.y (+ -2 (or self.sy 0)))))
                   ;;     (tset self :highlight 0)
                   ;; ))
                   
                   :pickup (fn [self player]
                             (local message (require :message))
                             ;; (print (.. "Pickup " self.name))
                             (local {: sounds} (require :assets))
                             (sounds.bounce:stop)
                             (sounds.bounce:play)
                             (player:add-ingredients self.name)
                             (love.event.push :pickup self.name)
                             (message.write (.. "Picked up " self.name))
                             (remove self))
                   :update
                   (fn [self dt]
                     (tset self :offset (+ self.offset (* self.speed dt)))
                     (when (> self.offset 8) (tset self :offset 0)))
                   
                   : set-highlight
                   :handlers {}
                   : draw-collider
                   : preview})

(tset ingredient :list
      [:Death
       :Life
       :Feather
       :Carrot
       :Seed
       :Nut
       :Rosemary
       :Garlic
       :Lemon
       :Lilly
       :Sunflower
       :Dragon
       :Hemp
       :Lavender
       :Root
       :Cherry])

(tset ingredient :titles
      {:Death "Death Fruit"
       :Life "Fruit of Life"
       :Feather "Feather Berry"
       :Carrot "Devine Carrot"
       :Seed "Giant Pumpkin Seed"
       :Nut "Ground Nut"
       :Rosemary "Etherial Rosemary"
       :Garlic "Just Garlic"
       :Lemon "Celestial Lemons"
       :Lilly "Lilly"
       :Sunflower "Sunflower"
       :Dragon "Dragon Hemp"
       :Hemp "Hemp"
       :Lavender "Lavender"
       :Root "Spirit Root"
       :Cherry "Transparent Cherries"})

(tset ingredient :descriptions
      {:Death "While not dangerous by this self, when mixed properly death fruit can be ... deadly"
       :Life "It looks like an apple, tastes like an apple, and if enchanted can walk like an apple."
       :Feather "This berry has some seriously lightening properties."
       :Carrot "The mythic wizards once said a devine carrot a day keeps fear of the dark away."
       :Seed "This is a seed of the now explant Giant Pumpkin."
       :Nut "These nuts grow out of the ground. Handy for picking"
       :Rosemary "Rosemary makes a good base for many potions"
       :Garlic "Garlic, now not just for doing in vampires!"
       :Lemon "When regular Leamons are not enough try Celestial Lemons."
       :Lilly "A simple lilly. Sometimes the magic comes from within."
       :Sunflower "Sunflowers make passers by smile."
       :Dragon "Dragon Hemp should be handled carefully. It is required for some of the greatest potions."
       :Hemp "Hemp makes a good base for many potions"
       :Lavender "Smells like your bedroom"
       :Root "Don't trip over the root."
       :Cherry "You can barely see these lil guys."})

(tset ingredient :timers (collect [_ key (ipairs ingredient.list)] (values key 0)))

(local ingredient-mt {:__index ingredient})

(fn ingredient.open [name]
  {:title (. ingredient.titles name)
   :name name
   :description (. ingredient.descriptions name)
   :recipie []
   :button false
   :visible true}
  )

(fn ingredient.create [brush]
  (local {: px : py : pw : ph : iw : ih : name} brush)
  (local {: atlas} (require :assets))
  (-> {:encode brush
       :name name
       :type :ingredient
       :offset 2 ;; (/ (math.random 800) 100)
       :speed 0 ;; (math.random 1 4)
       :quad (love.graphics.newQuad px py pw ph iw ih)
       :shadow-quad atlas.quads.Shadow1
       :shadow true
       :highlight 0
       :x brush.x :y brush.y
       :w brush.w :h brush.h}
      ((fn [x] (setmetatable x ingredient-mt)))
      (add-collider :cross)))


ingredient
