(local gamestate (require :lib.gamestate))
(local stdio (require :lib.stdio))
(local fade (require :lib.fade))
(local flux (require :lib.flux))

(fn love.load [args]
  ;; (collectgarbage "stop")
  (if (= :web (. args 1)) (global web true) (global web false))
  (if (= :dev (. args 1)) (global dev true) (global dev false))
  (require :handlers)
  (love.window.setTitle (.. "Potions - by alexjgriffith" (if dev " - DEV" "")))
  (love.graphics.setDefaultFilter "nearest" "nearest" 0)
  (local {: sounds : cursor-data} (require :assets))
  (when (not dev) (sounds.bgm:play))
  (gamestate.registerEvents)
  (if dev
      (do
        (local (w h flags) (love.window.getMode))
        (set flags.borderless true)
        (set flags.x 0)
        (set flags.y 0)
        (love.window.setMode w h flags)
        (gamestate.switch (require :mode-intro))) ;;"shader-test-level"
      (gamestate.switch (require :mode-opening)))
  ;; (gamestate.switch (require :mode-intro))
  ;; bug with web version? figure out
  (when (not web)
    (local cursor (love.mouse.newCursor cursor-data))
    (love.mouse.setCursor cursor))
  (when dev (stdio.start))
  )

(fn love.draw []
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.rectangle :fill 0 0 100 100))

(fn love.update [dt]
  (flux.update dt)
  (fade.update (math.min dt 0.032)))
