(local {: add-collider : draw-collider : preview} (require :prefab-object))

(local ford {:serialize
               (fn [self]  (print (.. "Encoding " self.name)) self.encode)
               :draw
               (fn [self image ...]
                 (love.graphics.draw image self.quad self.x self.y))
               :update
             (fn [self dt]
               (local {: player} (require :state))
               (if (> player.inventory.potions.MixtureOfLevitation 0)
                   (set self.down true)
                   (set self.down false)
                   )
               (if self.down
                   (set self.collider :cross)
                   (set self.collider :slide))
               )
               :handlers {}
               : draw-collider
               : preview})

(local ford-mt {:__index ford})

(fn ford.create [brush]
  (local {: px : py : pw : ph : iw : ih : name} brush)
  (local assets (require :assets))
   (-> {:encode brush
        :name name
        :type :ford
        :quad (love.graphics.newQuad px py pw ph iw ih)
        :x brush.x :y brush.y
        :w brush.w :h brush.h}
       ((fn [x] (setmetatable x ford-mt)))
       (add-collider :slide)))

ford
