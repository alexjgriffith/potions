(local assets (require :assets))
(local state (require :state))

(local lg love.graphics)

(local pallet (require :pallet))

(local {: in} (require :utils))
(local item-slots {})

(fn close-all-interfaces []
  (local interfaces (require :interface))
  (each [key value (ipairs interfaces)]
    (when (and (= :table (type value)) value.close)
      (value.close))))

(fn item-slots.draw [player scale]
  (local (mx my) (love.mouse.getPosition))
  (local ui assets.ui)
  (local atlas assets.atlas)
  (local {:list ingredient-list} (require :prefab-ingredients))
  (local {:list potion-list} (require :prefab-potions))

  (fn draw-potions [player name]
    (assert (= :Player player.name) "Missing Player")
    (local num (. player :inventory :potions name))
    (lg.push)
    (when (= num 0)
      (lg.translate player.potions-ox 0))
      (lg.push :all)
      (when (= num 0) (lg.setColor pallet.skin))
      (when (and  (player:brewable name) (= num 0))
        (lg.setColor 0 1 0 1))
      (if (in :potions mx my 16 16 name)
          (lg.draw ui.image ui.quads.SlotHover)
          (lg.draw ui.image ui.quads.Slot))
      (lg.pop)
      (lg.draw atlas.image (. atlas.quads name))
    (lg.push)
    (lg.push)
    (when (and player.can-brew (and (not (player:brewable name)) (= num 0)))
      (lg.draw ui.image ui.quads.Cross))
    (lg.pop)
    (lg.pop)
    (lg.pop)
    true)
  
  (fn draw-slot [type name]
    (lg.push :all)
    (if (and type name (in type mx my 16 16 name))
        (lg.draw ui.image ui.quads.SlotHover)
        (lg.draw ui.image ui.quads.Slot))
    (lg.pop))
  
  (fn draw-ingredient [player name]
    (assert (= :Player player.name) "Missing Player")
    (local num (. player :inventory :ingredients name))
    (var ret false)
    (when (> num 0)
      (set ret true)
      (lg.draw atlas.image (. atlas.quads name))
      (lg.push)
      (lg.translate 8 -2)
      (lg.draw ui.image ui.quads.HoverB)
      (lg.push :all)
      (lg.translate 3.5 0.5)
      ;; (lg.scale 0.5)
      (lg.setFont assets.font.text)
      (if (= num 0)
          (lg.setColor pallet.red)          
          (lg.setColor pallet.shadow))
      (lg.print num)
      (lg.pop)
      (lg.pop))
    ret)

  (lg.push :all)
  (local {: shadow} (require :pallet))
  (local {: font} (require :assets))
  (lg.setColor shadow)
  (lg.setFont font.header)
  (lg.translate 10 100)
  (lg.rotate (* math.pi (/ 3 2)))
  ;; (lg.push)
  ;; (lg.scale 0.25)
  (lg.print "POTIONS")
  ;; (lg.pop)
  (lg.pop)
  ;; potions
  (lg.push)
  (lg.translate 4 2)
  (for [i 1 9]
    (draw-potions player (. potion-list i))
    (lg.translate 0 18))
  (lg.pop)
  ;; ingredients
  (let [inventory (icollect [_ name (ipairs ingredient-list)]
                    (when (> (. player :inventory :ingredients name) 0)
                      name))]
    ;; (pp inventory)
    (lg.push)
    (lg.translate (+ 4 (* 16 (- 20 2.5))) 2)
    (var j 0)
    (for [i 1 16]
      (lg.push)
      (lg.translate (* (% j 2) 18) (* (math.floor (/ (+ j 0) 2)) 18))
      ;; (when (draw-ingredient player (. ingredient-list (+ 0 i)))
      ;;   (set j (+ j 1)))
      (local ing (. inventory i))
      (local hovered?
             (when ing
               (in :ingredients mx my 16 16 ing)))
      (lg.draw ui.image (if hovered? ui.quads.SlotHover ui.quads.Slot))
      
      (when ing
        (draw-ingredient player ing))
      (set j (+ j 1))
      (lg.pop))
    (lg.pop)
    )

  (lg.push :all)
  (lg.setFont assets.font.header)
  (lg.setColor pallet.shadow)
  (lg.translate (+ (* 16 18) -4) (+ (* 16 9) 4))
  ;; (lg.push)
  ;;(lg.scale 0.25)
  (lg.print (.. "OBJECTIVE"))
  ;; (lg.pop)
  (local objective (. state.goals state.day))
  ;; (pp state.day)
  (lg.setColor 1 1 1 1)
  (lg.translate 8 8)
  ;; (pp objective)
  (lg.draw atlas.image (. atlas.quads (or objective :Cat)))
  (when objective
    (in :potions mx my 16 16 objective))
  ;;(lg.print (.. "Day " state.day))
  (lg.setColor pallet.shadow)
  (lg.translate (- (- (* 16 19) 19)) 8)
  ;;(lg.print (.. "Potions"))
  (lg.setColor pallet.shadow)
  (when (in :button mx my 16 16 :Mute)
    (lg.setColor pallet.red))
  (if state.mute
      (lg.draw atlas.image atlas.quads.UnMute)
      (lg.draw atlas.image atlas.quads.Mute))
  
  (lg.pop)
  )

(fn item-slots.update [dt player])

(fn item-slots.hover [])

(local mixer-default {:title "" :description "" :recipie [] :button false
                      :visible false})
(local mixer-brew {:title "ExampleBrew"
                   :name :ElixerOfLife
                   :description "Reinvigorate your life, with this home brew elixir."
                   :recipie [:Dragon :Death :Life]
                   :button :Brew
                   :visible true})
(var mixer {:title "Empty" :name "EssenceOfStrength" :description "Empty" :recipie [] :button false :visible false})

;;(set mixer mixer-brew)

(fn mixer.open [type name situation]
  (close-all-interfaces)
  (local assets (require :assets))
  (assets.sounds.page:stop)
           (assets.sounds.page:play)
  (each [key value (pairs situation)]
    (tset mixer key value))
  (set mixer.ox -5)
  (set mixer.visible true))

(fn mixer.close []
  (each [key value (pairs mixer-default)]
    (tset mixer key value))
  (set mixer.visible false)
  )

(fn mixer.draw [player]
  (local (mx my) (love.mouse.getPosition))
  (local ui assets.ui)
  (local atlas assets.atlas)
  (local {:list ingredient-list} (require :prefab-ingredients))
  (local {:list potion-list} (require :prefab-potions))
  ;; (local {: player} (require :state))
  (lg.push)
  (lg.translate (* 16 5) 0)
  (if mixer.visible
      (lg.translate 0 48)
      (lg.translate 0 -200))
  (lg.draw ui.image ui.quads.Mixer)
  (lg.push)
  (lg.translate 0 2)
  (lg.push :all)
  (lg.setFont assets.font.header)
  (lg.setColor pallet.shadow)
  (lg.printf mixer.title 0 4 (* 16 9) :center)
  (lg.setFont assets.font.description)
  (lg.printf mixer.description 4 20 (- (* 16 9) 7) :left)
  (lg.pop)
  (lg.pop)
  (lg.push)
  (lg.translate 2 2)
  (lg.draw atlas.image (. atlas.quads mixer.name))
  (lg.pop)
  (when (> (# mixer.recipie) 0)
    (lg.push)
    (lg.translate (- (* 16 3) 2) (+ 8 (* 16 2)))
    (each [_ value (ipairs mixer.recipie)]
      (lg.draw atlas.image (. atlas.quads value))
      (lg.translate 18 0)
      )
    (lg.pop))

    (lg.push :all)
    (lg.translate (- (* 16 8) 4) 2)
    (local hover (in :button mx my 16 16  :x :mixer))
    (if hover
        (lg.draw ui.image ui.quads.XDown)
        (lg.draw ui.image ui.quads.XUp))
    (lg.pop)
  
    (when (and mixer.button (player:brewable mixer.name) player.can-brew (= (. player.inventory.potions mixer.name) 0))
      (lg.push :all)
      (lg.translate (* 16 3) (- (* 16 4) 4))
      (local hover (in :button mx my (* 16 3) 16  mixer.button mixer.name))
      (if hover
          (lg.draw ui.image ui.quads.ButtonDown)
          (lg.draw ui.image ui.quads.ButtonUp))
      (lg.setFont assets.font.text)
      (lg.setColor pallet.shadow)
      (lg.printf mixer.button 0 (if hover 5 4) (* 1 16 3) :center)
      (lg.pop))

    (when (and mixer.button (not (player:brewable mixer.name)) (= (. player.inventory.potions mixer.name) 0))
      (lg.push :all)
      (lg.translate (* 16 3) (- (* 16 4) 4))
      (lg.setFont assets.font.text)
      (lg.setColor pallet.shadow)
      (lg.printf "Find Ingredients" 0 0 (* 1 16 3) :center)
      (lg.pop))

    (when (and mixer.button (not player.can-brew) (player:brewable mixer.name) (= (. player.inventory.potions mixer.name) 0))
      (lg.push :all)
      (lg.translate (* 16 3) (- (* 16 4) 4))
      (lg.setFont assets.font.text)
      (lg.setColor pallet.shadow)
      (lg.printf "Move to pot" 0 0 (* 1 16 3) :center)
      (lg.pop))
    
    (local potions (require :prefab-potions))
    (local hint (. potions :hint mixer.name))
    (when (and hint  (> (. player.inventory.potions mixer.name) 0))
      (lg.push :all)
      (lg.translate 0 (- (* 16 4) 4))
      (lg.setFont assets.font.text)
      (lg.setColor pallet.shadow)
      (lg.printf hint -8  0 (* 10 16) :center)
      (lg.pop))
  
  (lg.pop))

(local text-box {:message "" :active true})

(fn text-box.open [message]
  (close-all-interfaces)
  (tset text-box :message message)
  (tset text-box :active true))

(fn text-box.close []
  (tset text-box :message "")
  (tset text-box :active false))

(fn text-box.draw []
  (when text-box.active
    (local (mx my) (love.mouse.getPosition))
    (local ui assets.ui)
    (local atlas assets.atlas)
    (lg.push :all)
    (lg.translate (* 16 5) 0)
    (lg.translate 0 48)
    ;; (lg.translate 0 -200)
    (lg.setColor 1 1 1 1)
    (lg.draw ui.image ui.quads.Mixer)
    (lg.push)
    (lg.translate (- (* 16 8) 4) 2)
    (local hover (in :button mx my 16 16  :x :text-box))
    (if hover
        (lg.draw ui.image ui.quads.XDown)
        (lg.draw ui.image ui.quads.XUp))
    (lg.pop)
    
    (lg.translate 0 2)
    (lg.setFont assets.font.header)
    (lg.setColor pallet.shadow)
    ;;(lg.push)
    ;;(lg.scale 0.25)
    (lg.printf (string.upper "The Elixir of Life") 0 (* 4 1) (* 16 9 1) :center)

    (lg.printf (string.upper "Basil your beloved cat is ill. Brew the elixir of life to cure him.

Beware the wicked wizard. Woo him with a love medley to begin your journey.")

               (* 1 6) (* 1 (+ 4 16)) (- (* 16 9 1) (* 1 6)) :left)
    ;;(lg.pop)
    (lg.pop)
    ))

(fn text-box.update [dt])


(fn init-hover []
  (local state (require :state))
  (when (or (not state.hover-count) (= state.hover-count 0))
    (set state.hover nil))
  (tset state :hover-count 0))

{: item-slots : init-hover : text-box : mixer}
