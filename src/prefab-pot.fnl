(local {: add-collider : draw-collider : preview : draw-shadow
        : set-highlight
        : add-sprite-pipeline-canvases
        : wrap-grass-shader}
       (require :prefab-object))

(local {: draw-animation : animation-update} (require :animation))

(fn draw [self image ...]
  (local {: grass-shader-pipeline} (require :prefab-object))
  (wrap-grass-shader self (fn []
                            ;; (local {: atlas} (require :assets))
                            (local lg love.graphics)
                            (lg.push)
                            (lg.translate self.x self.y)
                            (draw-animation self.animation)
                            (draw-animation self.spoon)
                            (lg.pop))))

(fn update [self dt]
  (fn is-intersecting [self player]
    (rect_isIntersecting player.x player.y player.w player.h
                         (- self.x 0) (- self.y 0) 16 16))
  (local {: player} (require :state))
  (let [active (is-intersecting self player)
        next-state (if active :stir :idle)]
    (tset self.spoon :state-change (= next-state self.animation.state))
    (tset self.spoon :state next-state))
  (local near-by (rect_isIntersecting player.x player.y player.w player.h
                                  (- self.x 64) (- self.y 64) 128 128))
  (when near-by (player:at-pot (= self.spoon.state :stir)))
  (animation-update dt self.animation)
  (animation-update dt self.spoon))

(local pot {:serialize
            (fn [self]  (print (.. "Encoding " self.name)) self.encode)
            :handlers {}
            : draw-collider
            : preview
            : draw-shadow
            : draw
            : set-highlight
            : update
            :brew-potion (fn [self])
            })


(local pot-mt {:__index pot})

(fn pot.create [brush]
  (local {: px : py : pw : ph : iw : ih : name} brush)
  (local assets (require :assets))
  (local aq assets.atlas.quads)
   (-> {:encode brush
        :name name
        :animation {:index 1 :duration 0.2 :timer 0 :state :idle :state-change false :durations {:idle 0.2}
                    :quads {:idle [aq.Pot3 aq.Pot3 aq.Pot1 aq.Pot2 aq.Pot2 aq.Pot2 aq.Pot1]}
                    :image assets.atlas.image}
        :spoon {:index 1 :duration 0.3 :timer 0 :state :idle :state-change false :durations {:idle 0.3 :stir 0.2}
                :quads {:idle [aq.Handle1]
                        :stir [aq.Handle1 aq.Handle2 aq.Handle3]}
                :image assets.atlas.image}
        :type :pot
        :highlight 0
        :shadow true
        :shadow-quad aq.Shadow1
        :sy 2
        :quad (love.graphics.newQuad px py pw ph iw ih)
        :x brush.x :y brush.y
        :w brush.w :h brush.h}
       ((fn [x] (setmetatable x pot-mt)))
       add-sprite-pipeline-canvases
       (add-collider :slide)))

pot
