(local fade (require :lib.fade))
(local gamestate (require :lib.gamestate))

(local pallet (require :pallet))
(local assets (require :assets))
(local lg love.graphics)

(fn update [gs dt])

(fn enter [gs]  
  (fade.in))

(fn prepare-buttons []
  (local interface (require :interface))
  (interface.init-hover)
  (local (mx my) (love.mouse.getPosition))
  (values mx my))

(fn button [text mx my w h callback]
  (local (screen-mx screen-my) (lg.inverseTransformPoint mx my))
  (local assets (require :assets))
  (local hover (_G.pointWithin screen-mx screen-my 0 0 w h))
  (when hover
    (love.event.push :hover :play)
    (when (and hover (love.mouse.isDown 1))
      (assets.sounds.page:stop)
      (assets.sounds.page:play)
      (callback)))
  (lg.setColor 1 1 1 1)
  (lg.draw assets.ui.image (if hover
                               assets.ui.quads.ButtonDown
                               assets.ui.quads.ButtonUp) 0 0)
  (lg.setFont assets.font.text)
  (lg.setColor pallet.shadow)
  (lg.printf text 0 (if hover 4 3) w :center)
  (lg.setColor 1 1 1 1))

(fn draw [gs]  
  (lg.push :all)
  (lg.scale 4)
  (lg.clear pallet.skin)
  (lg.setColor pallet.shadow)
  (lg.translate 0 20)
  (lg.setFont assets.font.main-title)
  (lg.printf (string.upper "You Win!") 0 0 (/ 1280 4) :center)
  (lg.setFont assets.font.subtitle)
  (lg.printf (string.upper "You Successfully Brewed the Elixir of Life and Saved Basil the Cat!") 0 30 (/ 1280 4) :center)
  
  (lg.push)
  (lg.setColor [1 1 1 1])
  (lg.translate 128 (+  96 16))
  (local (w h) (values 48 16))
  (var (forcolour backcolour) (values pallet.shadow [1 1 1 0]))
  (local (mx my) (prepare-buttons))
  (button "Quit Game" mx my w h
          (fn [] (love.event.quit)))
  (lg.translate 0 20)
  (button "Replay" mx my w h
          (fn [] (fade.out (fn [] (gamestate.switch (require :mode-intro))))))

  
  
  (lg.pop)
  (lg.push :all)
  (lg.setColor pallet.shadow)
  (lg.translate 0 136)
  (lg.setFont assets.font.text)
  (lg.printf "Game By: AlexJGriffith" 20 0 (/ 1280 4) :left)
  (lg.pop)
  (lg.translate 300 250)
  (lg.origin)
  (lg.setColor 1 1 1 1)
  (lg.draw fade.canvas)
  (lg.pop)
)


{: draw : update : enter}
