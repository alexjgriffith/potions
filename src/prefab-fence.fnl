(local {: draw-collider : preview} (require :prefab-object))

(local fence {:serialize
               (fn [self]  (print (.. "Encoding " self.name)) self.encode)

               :draw
              (fn [self image ...] (love.graphics.draw image
                                                       (if self.down
                                                           self.down-quad
                                                           self.quad)
                                                        self.x self.y))
               :update
              (fn [self dt]
                ;; (pp :fence)
                (local state (require :state))
                (local buttons (. (state.level.get-layer state.level :buttons) :data))
                (local linked (match self.name
                                :GreenFence :GreenButton
                                :RedFence :RedButton
                                :BlueFence :BlueButton
                                :YellowFence :YellowButton))
                ;; (pp [self.name linked])
                (var pressed false)
                (each [_ button (ipairs buttons)]
                  ;; (pp [button.down button.name linked] )
                  (when (and button.down (= button.name linked))
                    ;; (pp [button.down button.name linked] )
                    (set pressed true)))
                (when (~= self.down pressed)
                  ;; (pp pressed)
                  (love.event.push :fence-moved pressed))
                (tset self :down pressed)
                (set self.collider (if self.down :cross :slide))
                )
              : draw-collider
              : preview
              :handlers {}})

(local fence-mt {:__index fence})

(fn fence.create [brush]
  (local {: px : py : pw : ph : iw : ih : name} brush)
  (local {: add-collider} (require :prefab-object))
  (local assets (require :assets))
  (-> {:encode brush
       :name name
       :quad (love.graphics.newQuad px py pw ph iw ih)
       :down-quad (. assets.atlas.quads (.. name :Down))
       :down false
       :x brush.x :y brush.y       
       :w brush.w :h brush.h}
      ((fn [x] (setmetatable x fence-mt) x))
      add-collider))


fence
