(local {: add-collider : draw-collider : preview
        : draw-shadow
        : set-highlight
        : grass-shader-pipeline
        : add-sprite-pipeline-canvases}
       (require :prefab-object))

(local {: draw-animation : animation-update} (require :animation))

(local messages
       {:no-love-medley ["Whoa slow down there mister!"
                         "Before I will let you leave you must brew a Love Medley!"
                         "The ingredients can be found in your yard"
                         "Press space to pick them up"
                         "Go to the pot to brew potions."
                         "Your list of brewable potions will be on the left."
                         ]
        :love-medley ["Thank you. Now go save your cat!"
                      "To lift stones you need the Essence of Strength"
                      "Good luck!"]
        :essence-of-strength
        ["To cross rivers you need the Mixture of Levitation"
         "Good luck!"]})

(fn update-state [self player world]
  (fn is-intersecting [self player]
    (rect_isIntersecting player.x player.y player.w player.h
                         (- self.x 8) (- self.y 16) (+ 16 16) 64))
  (local state
         (if self.given-love-potion
             :love
             (is-intersecting self player)
             :block
             :idle))
  (local last-state self.animation.state)
  (local state-change (~= state last-state))
  (set self.animation.state state)
  (set self.animation.state-change state-change))

(local wizard {:serialize
               (fn [self]  (print (.. "Encoding " self.name)) self.encode)

               : set-highlight
               : draw-shadow                
               :handlers {}
               :talk (fn [self player]
                       (when (> player.inventory.potions.LoveMedley 0)
                         (tset self :potion-state :love-medley))
                       (when (> player.inventory.potions.EssenceOfStrength 0)
                         (tset self :potion-state :essence-of-strength))
                       (tset  self.message-index self.potion-state (+ (. self.message-index self.potion-state) 1))
                       (when (> (. self.message-index self.potion-state) (# (. messages self.potion-state)))
                         (tset self.message-index self.potion-state 1))
                       ;; (pp :talk-wizard)
                       
                       (tset self :timer 5))
               :give-love-potion
               (fn [self]
                 (tset self :given-love-potion true))
               : draw-collider
               : preview})

(fn draw-callback [self]
  (fn []
    (local lg love.graphics)
    (lg.push)
    ;; (lg.translate (if self.left (+ self.x 16) self.x) (- self.y self.target.y))
    ;; (lg.scale (if self.left -1 1) 1)
    (lg.setColor 1 1 1 1)
    (draw-animation self.animation)
    (when self.given-love-potion
      (local {: atlas} (require :assets))
      (lg.draw atlas.image atlas.quads.Heart 4 -6))
    (lg.pop)))

(fn wizard.draw [self image ...]
  (local {: grass-shader-pipeline} (require :prefab-object))
  (grass-shader-pipeline
   self
   (draw-callback self)
   self.target.x
   self.target.y))

(fn wizard.draw-water [self image ...]
  (local {: water-flip-pipeline} (require :prefab-object))
  (water-flip-pipeline
   self
   (draw-callback self)
   self.target.x
   self.target.y))

;; co-opted for text
(fn wizard.draw-outline [self image]
  (local lg love.graphics)
     (when (> self.timer 0)
       (local {: shadow} (require :pallet))
       (local {: ui : font} (require :assets))
       (lg.push :all)
       (local shader (lg.getShader))
       (lg.setShader)
       (lg.setColor 1 1 1 1)
       (lg.translate (if self.left (+ self.x 16) self.x) (- self.y self.target.y))
       (local message (. messages self.potion-state (. self.message-index self.potion-state)))
       (local width (math.ceil (/ (font.inline:getWidth (.. message "    ")) 16)))
       ;;(lg.draw ui.image ui.quads.WizardTextBox 2 -18)
       (lg.push)
       (lg.translate 2 -18)
       (lg.draw ui.image ui.quads.WizardTextBox1)
       (for [i 1 (- width 2)]
         (lg.translate 16 0)
         (lg.draw ui.image ui.quads.WizardTextBox2))
       (lg.translate 16 0)
       (lg.draw ui.image ui.quads.WizardTextBox3)
       (lg.pop)
       
       (lg.scale 1)
       (lg.setColor shadow)
       (lg.setFont font.inline)
       (lg.print (string.upper message)  6 -14)
       (lg.setShader shader)
       (lg.pop)))

(fn wizard.update [self dt]
  (local {: player : world} (require :state))
  (if (< player.x self.x)
      (tset self :left true)
      (tset self :left false))
  (update-state self player world)
  (animation-update dt self.animation)
  (if self.given-love-potion
      (set self.collider :cross)
      (set self.collider :slide))
  (when (= self.animation.state :block)
    (tset self.target :ty (- self.y player.y)))
  (set self.target.y (lume.lerp self.target.y self.target.ty 0.2))
  (set self.sy (- self.sy-default self.target.y))
  (set self.timer (- self.timer dt)))


(local wizard-mt {:__index wizard})

(fn wizard.create [brush]
  (local {: px : py : pw : ph : iw : ih : name} brush)
  (local assets (require :assets))
  (local aq assets.atlas.quads)
   (-> {:encode brush
        :name name
        :type :wizard
        :animation {:state :idle
                    :duration 0.5
                    :timer 0
                    :index 1
                    :durations {:block 0.5 :idle 0.5 :love 0.5}
                    :state-change false
                    :image assets.atlas.image
                    :quads {:block [aq.WizardBlock1 aq.WizardBlock2]
                           :idle [aq.WizardIdle1 aq.WizardIdle2]
                           :love [aq.WizardLove1 aq.WizardLove2]}}
        
        :shadow true
        :shadow-quad aq.Shadow
        :timer 0
        :sy-default 2
        :sy 2
        :left false
        :highlight 0
        :quad (love.graphics.newQuad px py pw ph iw ih)
        :x brush.x :y brush.y
        :w brush.w :h brush.h
        :canvas-offset nil
        :potion-state :no-love-medley
        :message-index {:no-love-medley 0 :love-medley 0 :essence-of-strength 0}
        :target {:tx 0 :ty 0 :x 0 :y 0}}
       ((fn [x] (setmetatable x wizard-mt)))
       (add-collider :slide)
       add-sprite-pipeline-canvases))

wizard
