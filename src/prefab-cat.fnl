(local {: add-collider : draw-collider : preview : draw-shadow
        : set-highlight
        : add-sprite-pipeline-canvases}
       (require :prefab-object))

(local {: draw-animation : animation-update} (require :animation))

(fn draw-callback [self]
  (fn []
    (local {: ui : font} (require :assets))
    (local {: shadow} (require :pallet))
    (local lg love.graphics)
    (lg.push)
    (when (> self.timer 0)
      (lg.push :all)
      (lg.draw ui.image ui.quads.CatTextBox -16 -14)
      (lg.scale 1)
      (lg.setColor shadow)
      (lg.setFont font.inline)
      (lg.printf "Meaw!" -16 -10 48 :center)
      (lg.pop))
    (draw-animation self.animation)
    (lg.pop)))

(fn draw [self image ...]
  (local {: grass-shader-pipeline} (require :prefab-object))
  (grass-shader-pipeline self (draw-callback self)))

(fn draw-water [self image ...]
  (local {: water-flip-pipeline} (require :prefab-object))
  (water-flip-pipeline self (draw-callback self)))

(fn update [self dt]
  (tset self :timer (- self.timer dt))
  (animation-update dt self.animation))

(local cat {:serialize
            (fn [self]  (print (.. "Encoding " self.name)) self.encode)
            :handlers {}
            : draw-collider
            : preview
            : draw-shadow
            : draw
            : draw-water
            : set-highlight
            : update
            :pet (fn [self] (set self.timer 3))
            :give-potion (fn [self]
                           (local {: sounds} (require :assets))
                           (local sound (. sounds :meaw))
                           (sound:stop)
                           (sound:play)
                           (love.event.push :game-over :ElixerOfLife))
            })


(local cat-mt {:__index cat})

(local duration {:idle 0.7})

(fn cat.create [brush]
  (local {: px : py : pw : ph : iw : ih : name} brush)
  (local assets (require :assets))
  (local aq assets.atlas.quads)
   (-> {:encode brush
        :name name
        :timer 0
        :animation {:index 1 :duration 0.7 :timer 0 :state :idle :state-change false :durations duration
                    :quads {:idle [aq.Cat1 aq.Cat2]}
                    :image assets.atlas.image}
        :type :cat
        :highlight 0
        :sy brush.sy
        :shadow-quad (. aq brush.shadow)
        :shadow true
        :quad (love.graphics.newQuad px py pw ph iw ih)
        :x brush.x :y brush.y
        :w brush.w :h brush.h}
       ((fn [x] (setmetatable x cat-mt)))
       add-sprite-pipeline-canvases
       (add-collider :slide)))

cat
