(local potions {})

(tset potions :list
      [:LoveMedley
       :EssenceOfStrength
       :CauseOfDeath
       :MixtureOfLevitation
       :BrewOfBed
       :DragonHideCompound
       :InvisibilityPotion
       :UnholyConcoction
       :ElixerOfLife])

(tset potions :recipies
      {:ElixerOfLife [:Dragon :Life :Sunflower] ;; Life ;; done
       :MixtureOfLevitation [:Hemp :Feather :Carrot] ;; Feather ;; done
       :CauseOfDeath [:Carrot :Lilly :Death] ;; Death ;; done
       :DragonHideCompound [:Dragon :Root :Hemp] ;; Root ;; Done
       :BrewOfBed [:Rosemary :Lilly :Lavender] ;; Lavender ;; Done
       :UnholyConcoction [:Dragon :Hemp :Garlic] ;; Garlic ;; Done
       :EssenceOfStrength [:Carrot :Hemp :Nut] ;; Nut ;; Done
       :LoveMedley [:Hemp :Lilly :Seed] ;; Seed ;; done
       :InvisibilityPotion [:Lemon :Sunflower :Cherry]} ;; Cherry ;; done
      )

(tset potions :titles
      {:ElixerOfLife "Elixir Of Life"
       :MixtureOfLevitation "Mixture Of Levitation"
       :CauseOfDeath "Cause Of Death"
       :DragonHideCompound "Dragon Hide Compound"
       :BrewOfBed "Brew Of Bed"
       :UnholyConcoction "Unholy Concoction"
       :EssenceOfStrength "Essence Of Strength"
       :LoveMedley "Love Medley"
       :InvisibilityPotion "Invisibility Potion"})

(tset potions :descriptions
      {:ElixerOfLife "This lil home brew will keep you alive way longer than you'd like to be."
       :MixtureOfLevitation "Ever thought of walking on water? How about over really shallow water?"
       :CauseOfDeath "Don't worry it won't kill you. It is useful for removing stumps through."
       :DragonHideCompound "Gives you the hide of a dragon."
       :BrewOfBed "Teleports you to where you last slept."
       :UnholyConcoction "Sell this, don't drink it!"
       :EssenceOfStrength "If small rocks are to heavy for you try this drought."
       :LoveMedley "Just like your favourite song this drink will make you feel things."
       :InvisibilityPotion "Hide from your responsibilities with this handy potion."})

(tset potions :hint
      {:EssenceOfStrength "Lets you lift small stones!"
       :CauseOfDeath "Lets you cut through Logs!"
       :MixtureOfLevitation "Lets you levitate over river stones!"
       :LoveMedley "The Wizard was looking for this!"
       :ElixerOfLife "Needed to save Basil the cat!"})


;; timer for wiggling viable potions
(tset potions :timers (collect [_ key (ipairs potions.list)] (values key {:wait 3 :shake 1 :timer 0 :state :wait :active false})))

(fn potions.open [name]
  {:title (. potions.titles name)
   :name name
   :description (. potions.descriptions name)
   :recipie (. potions.recipies name)
   :button :Brew
   :visible true}
  )

potions
