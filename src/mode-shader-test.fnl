(local assets (require :assets))

(var (x y) (values 0 0))
(var (cx cy) (values 0 0))

(local green (lume.map [75 105 47 256] (fn [x] (/ x 256))))

(fn update [gs dt]  
  (let [isDown (fn [b] (if (love.keyboard.isDown b) 1 0))
        l (isDown :left)
        r (isDown :right)
        u (isDown :up)
        d (isDown :down)
        vxp (- r l)
        vyp (- d u)
        speed 100
        m (math.sqrt (+ (^ vxp 2) (^ vyp 2)))
        vx (if (~= 0 m) (/ vxp m) 0)
        vy (if (~= 0 m) (/ vyp m) 0)]
    (set x (+ x (* dt speed vx)))
    (set y (+ y (* dt speed vy))))

  (let [isDown (fn [b] (if (love.keyboard.isDown b) 1 0))
        l (isDown :a)
        r (isDown :d)
        u (isDown :w)
        d (isDown :s)
        vxp (- r l)
        vyp (- d u)
        speed 100
        m (math.sqrt (+ (^ vxp 2) (^ vyp 2)))
        vx (if (~= 0 m) (/ vxp m) 0)
        vy (if (~= 0 m) (/ vyp m) 0)]
    (set cx (+ cx (* dt speed vx)))
    (set cy (+ cy (* dt speed vy))))
  )

(local mask-canvas (love.graphics.newCanvas 1280 720))
(local grass-shader (love.graphics.newShader :assets/shaders/grass.glsl))
(fn init [gs]
  (local lg love.graphics)
)

(fn draw-obj [what x y]
  (local lg love.graphics)
  (assert (. assets.atlas.quads what) (.. what " is not a valid object name."))
  (lg.setShader grass-shader)
  (local ({:px px :py py :pw pw :ph ph :iw iw :ih ih})
         (. assets.atlas.brushes what))
  (grass-shader:send :spriteSize [pw ph])
  (grass-shader:send :spriteAtlasPosition [px py])
  (grass-shader:send :atlasSize [iw ih])
  (grass-shader:send :mask mask-canvas)
  (grass-shader:send :grassCanvasSize [1280 720])
  (grass-shader:send :camera [cx cy])
  (grass-shader:send :scale 4)
  (lg.draw assets.atlas.image (. assets.atlas.quads what) x y)
  (lg.setShader))

(fn draw [gs]
  (local lg love.graphics)
  (lg.push :all)
  (lg.setCanvas mask-canvas)
  (lg.setColor 1 1 1 1)
  (lg.draw assets.masks.image assets.masks.quads.Wheat)
  (lg.setCanvas)
  (lg.clear green)
  (lg.setColor 1 1 1 1)
  (lg.scale 4)
  (lg.translate cx cy)
  (lg.draw assets.atlas.image assets.atlas.quads.Wheat)
  (draw-obj :BigTree 32 0)
  (draw-obj :SmallStone 0 0)
  (draw-obj :Player x y)
  (lg.pop))

{: draw : update}
