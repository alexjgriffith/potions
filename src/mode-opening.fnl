(local fade (require :lib.fade))
(local gamestate (require :lib.gamestate))

(local pallet (require :pallet))
(local assets (require :assets))
(local lg love.graphics)

(fn update [gs dt])

(fn enter [gs] (fade.in))

(fn draw [gs]  
  (lg.push :all)
  (lg.scale 4)
  (lg.clear pallet.skin)
  (lg.setColor pallet.shadow)
  (lg.translate 0 20)
  (lg.setFont assets.font.main-title)
  (lg.printf (string.upper "Potions") 0 0 (/ 1280 4) :center)
  (lg.setFont assets.font.text)
  (lg.printf "Brew the Elixer of Life" 0 26 (/ 1280 4) :center)
  (lg.printf (string.upper "Controls") 0 56 (/ 1280 4) :center)
  (lg.draw assets.controls 116 64)
  (lg.push)
  (lg.setColor [1 1 1 1])
  (lg.translate 128 (+ 32 96))
  (local (w h) (values 48 16))
  (var (forcolour backcolour) (values pallet.shadow [1 1 1 0]))
  (local (mx my) (love.mouse.getPosition))
  (local (screen-mx screen-my) (lg.inverseTransformPoint mx my))
  (local interface (require :interface))
  (interface.init-hover)
  (local hover (_G.pointWithin screen-mx screen-my 0 0 w h))
  (when hover
    (love.event.push :hover :play)
    (when (and hover (love.mouse.isDown 1))
      (assets.sounds.page:stop)
      (assets.sounds.page:play)
      (fade.out (fn [] (gamestate.switch (require :mode-intro)))
                {:text :LOADING :font assets.font.main-title :ellipsis "" :timer 0})
      ))
  (lg.draw assets.ui.image (if hover
                               assets.ui.quads.ButtonDown
                               assets.ui.quads.ButtonUp) 0 0)
  (lg.setFont assets.font.text)
  (lg.setColor pallet.shadow)
  (lg.printf "Play" 0 (if hover 4 3) w :center)
  (lg.pop)
  (lg.push :all)
  (lg.setColor pallet.shadow)
  (lg.translate 0 136)
  (lg.setFont assets.font.text)
  (lg.printf "Game By: AlexJGriffith" 20 0 (/ 1280 4) :left)
  (lg.pop)
  (lg.translate 300 250)
  (lg.origin)
  (lg.setColor 1 1 1 1)
  (lg.draw fade.canvas)
  (lg.pop)

)


{: draw : update : enter}
