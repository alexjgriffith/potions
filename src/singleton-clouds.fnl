(local cloud-prefix :Cloud)
(local available-clouds 5)
(local unique-clouds 64)
(local max-clouds 256)
(local (volume-x volume-y) (values (* (- (* 16 4) 3) 16)
                                   (* (- (* 11 4) 3) 16)))
(local cloud-canvas-size 64)
(local cloud-canvas (love.graphics.newCanvas
                     cloud-canvas-size
                     (* cloud-canvas-size unique-clouds)))

(local cloud-index [])
 
(var wind [0 0])

(fn create-cloud [atlas]
  (local member-count (math.random 2 4))
  (for [i 1 member-count]
    (let [cloud-name (.. cloud-prefix (math.random available-clouds))
          brush (. atlas :brushes cloud-name)
          quad (. atlas :quads cloud-name)
          image (. atlas :image)
          x1 (-> brush.w (/ 2) (math.floor))
          y1 (-> brush.h (/  2) (math.floor))
          x2 (- cloud-canvas-size (* x1 2))
          y2 (- cloud-canvas-size (* y1 2))
          x (math.random x1 x2)
          y (math.random y1 y2)]
      (love.graphics.draw image quad x y)
      ;; (love.graphics.circle :fill 0 0 20)
      ;; (print (.. "Draw " x " " y))
      )))

(fn single-cloud [atlas ?canvas]
  (local lg love.graphics)
  (local canvas (or ?canvas
                    (lg.newCanvas cloud-canvas-size cloud-canvas-size)))
  (lg.push :all)
  (lg.setCanvas canvas)
  (create-cloud atlas)
  (lg.setCanvas)
  (lg.pop)
  canvas)

(fn init [atlas]
  "Draws all clouds to cloud-canvas and initializes cloud index with
the position of all the clouds and their quads."
    (local lg love.graphics)
    (lg.push)
    (lg.setColor 1 1 1 1)
    (lg.setCanvas cloud-canvas)
    ;; (lg.circle :fill 0 0 100)
    (for [i 1 max-clouds]
      (create-cloud atlas)
      ;; (lg.circle :fill 0 0 100)
      (lg.translate 0 cloud-canvas-size)
      (tset cloud-index i
            {:cloud (math.random unique-clouds)
             :x (math.random volume-x)
             :y (math.random volume-y)
             :speed 1
             :quad
             (lg.newQuad 0 (* (- i 1) cloud-canvas-size)
                         cloud-canvas-size cloud-canvas-size
                         cloud-canvas-size
                         (* cloud-canvas-size unique-clouds)
                         )}))
    (lg.setCanvas)
    (lg.pop)
    (when dev
      (let [data (cloud-canvas:newImageData)]
        (data:encode "png" "filename.png")))
    )

(fn draw []
  ;; optimize with spritebatch in future
  ;; (when (= (# cloud-index) 0) (init))
  (local lg love.graphics)
  (lg.push :all)
  (lg.setColor 1 1 1 0.2)
  (each [_i cloud (ipairs cloud-index)]
    ;; (lg.circle :fill cloud.x cloud.y 20)
    (lg.draw cloud-canvas cloud.quad cloud.x cloud.y))
  (lg.pop)
  )

(fn update [dt]
  ;; (when (= (# cloud-index) 0)
  ;;   (init))
  )

(fn clear []
  (each [k v (ipairs cloud-index)]
    (tset v :active false))
  (love.graphics.setCanvas cloud-canvas)
  (love.graphics.clear)
  (love.graphics.setCanvas))

(fn draw-cloud-canvas [...]
  (love.graphics.draw cloud-canvas ...))

{: init : draw : update : clear : single-cloud : draw-cloud-canvas}
