(local {: draw-collider : preview : draw-shadow} (require :prefab-object))

(fn move-filter [item other]
  (or other.collider :slide))

(local {: atlas} (require :assets))

(local idle-outline [atlas.quads.Idle1Shadow atlas.quads.Idle2Shadow])
(local run-outline [atlas.quads.Run1Shadow atlas.quads.Run2Shadow])

(local over-head-timing 0.5)

(fn move-match [col]
  (each [_ c (ipairs col)]
    (match c.other.type
      :ingredient :nothing ;; (c.other:pickup c.item)
      :button :nothing ;;(pp c.other.name)
      )))

(fn update-direction [player vx vy]
  (if (> vx 0) (do (set player.right true) (set player.left false))
      (< vx 0) (do (set player.right false) (set player.left true))
      (> vy 0) (do (set player.up false) (set player.down true))
      (< vy 0) (do (set player.up true) (set player.down false))))

(fn update-abilities [player]
  (fn update-ability [player name active]
    (let [current-state (. player :abilities name)]
      (when (= current-state active)
        (tset (. player :abilities) name active)
        (love.event.push :ability name active))))
  (each [_ [ability potion]
         (ipairs
          [[:cut player.inventory.potions.CauseOfDeath]
           [:strength player.inventory.potions.EssenceOfStrength]
           [:levitate player.inventory.potions.MixtureOfLevitation]])]
    (update-ability player ability (> potion 0))))

(fn cut-stump [player targets]
  (when (and (not player.hands) targets.stump (= player.inventory.potions.CauseOfDeath 0))
    (love.event.push :missing-action :CauseOfDeath))
  (when (and (not player.hands) targets.stump (> player.inventory.potions.CauseOfDeath 0))
    ;; (set player.inventory.potions.CauseOfDeath (- player.inventory.potions.CauseOfDeath 1))
    (targets.stump:remove)
    true))

(fn talk-wizard [player targets]
  (when (and targets.wizard (= player.inventory.potions.LoveMedley 0))
    (targets.wizard:talk player)
    (love.event.push :missing-action :LoveMedley))
  (when (and targets.wizard (> player.inventory.potions.LoveMedley 0))
    ;; (set player.inventory.potions.CauseOfDeath (- player.inventory.potions.CauseOfDeath 1))
    (targets.wizard:give-love-potion)
    (targets.wizard:talk player)
    true))

(fn talk-cat [player targets]
  (when (and targets.cat (= player.inventory.potions.ElixerOfLife 0))
    (targets.cat:pet)
    (love.event.push :missing-action :ElixerOfLife :meaw))
  (when (and targets.cat (> player.inventory.potions.ElixerOfLife 0))
    (targets.cat:give-potion)
    true))

(fn check-ford [player targets]
  (when (and (not player.hands) targets.ford (= player.inventory.potions.MixtureOfLevitation 0))
    (love.event.push :missing-action :MixtureOfLevitation))
  (when (and (not player.hands) targets.ford (> player.inventory.potions.MixtureOfLevitation 0))
    true))

(fn get-stone [player targets]
  (when (and (not player.hands) targets.stone (= player.inventory.potions.EssenceOfStrength 0))
    (love.event.push :missing-action :EssenceOfStrength))
  (when (and (not player.hands) targets.stone (> player.inventory.potions.EssenceOfStrength 0))
    ;; (set player.inventory.potions.EssenceOfStrength (- player.inventory.potions.EssenceOfStrength 1))
    (set player.hands targets.stone)
    (targets.stone:remove)
    true))

(fn get-ingredient [player targets]
  (when (and (not player.hands) targets.ingredient )
    ;;(love.event.push :missing-action :EssenceOfStrength)
    :nothing
    )
  (when (and (not player.hands) targets.ingredient)
    ;; (set player.inventory.potions.EssenceOfStrength (- player.inventory.potions.EssenceOfStrength 1))
    (targets.ingredient:pickup player)
    true))

(fn put-stone-button [player targets]
  (when (and targets.button player.hands)
    (set player.hands.x (+ targets.button.x 0))
    (set player.hands.y (+ targets.button.y -3))
    (player.hands:re-add)
    (tset player :hands nil)
    true))

(fn put-stone [player targets]
  (when player.hands
    (set player.hands.x (+ player.x 0))
    (set player.hands.y (+ player.y 4))
    (player.hands:re-add)
    (tset player :hands nil)
    true))

(fn update-state [player vx vy last-state]
  (local state
         (if (> (+ (math.abs vx) (math.abs vy)) 0)
             :run
             :idle))
  (local state-change (~= state last-state))
  (set player.state state)
  (set player.state-change state-change))

(fn reset-animation [player state]
  (set player.durration (. player.durrations state))
  (set player.timer 0)
  (set player.index 1))

(fn update-animation [player dt]
  (when player.state-change
    (reset-animation player player.state))
  (set player.timer (+ player.timer dt))
  (when (> player.timer player.durration)
    (set player.timer 0)
    (set player.index (+ player.index 1)))
  (when (> player.index 2)
    (set player.index 1)))

;; (local grass-shader (love.graphics.newShader :assets/shaders/grass.glsl))

(local interaction-types {:stone true :stump true :button true :ford true :wizard true :ingredient true :cat true})

(local player {:serialize
               (fn [self]  (pp :encoding-player) self.encode)

               :set-highlight
               (fn [self]
                 (local {: world} (require :state))
                 (local (x y w h) (if self.left
                                      (values (- (+ self.x 8) 20) (- self.y 3) 20 26)
                                      (values  (+ self.x 8) (- self.y 3) 20 26)))
                 (local (items _) (world:queryRect x y w h
                                                   (fn [item] (. interaction-types item.type))))
                 (local targets (collect [key _ (pairs interaction-types)] (values key false)))
                 (each [_ item (ipairs items)]
                   (when item.set-highlight (item:set-highlight))))
               
               :interact
               (fn [self]
                 (local {: world} (require :state))
                 (local (x y w h) (if self.left
                                      (values (- (+ self.x 8) 20) (- self.y 3) 20 26)
                                      (values  (+ self.x 8) (- self.y 3) 20 26)))
                 (local (items _) (world:queryRect x y w h
                                                   (fn [item] (. interaction-types item.type))))
                 (local targets (collect [key _ (pairs interaction-types)] (values key false)))
                 (each [_ item (ipairs items)]
                   (tset targets item.type item))
                 (when (or (get-ingredient self targets) (cut-stump self targets) (get-stone self targets)
                           (put-stone-button self targets) (talk-wizard self targets) (talk-cat self targets) (put-stone self targets)
                           (check-ford self targets))
                   (local {: sounds} (require :assets))
                   (sounds.bounce:stop)
                   (sounds.bounce:play)
                   true)
                 )
               
               :draw
               (fn [self image ...]
                 (local lg love.graphics)
                 (lg.setColor 1 1 0 0.2)
                 ;; (if self.left
                 ;;     (lg.rectangle :fill (- (+ self.x 8) 20) (- self.y 3) 20 26)
                 ;;     (lg.rectangle :fill (+ self.x 8) (- self.y 3) 20 26))
                 (lg.setColor 1 1 1 1)                 
                 (local quad (match [(or self.hands (when (> self.picked-up 0) true)) self.state]
                               [nil :idle] (. self.idle-quad self.index)
                               [_ :idle] (. self.idle-quad-up self.index)
                               [nil :run] (. self.run-quad self.index)
                               [_ :run] (. self.run-quad-up self.index)
                               ))
                 ;;(lg.draw image shadow-quad)
                 ;; (pp shadow-quad)
                 ;; FIXME - use object draw-grass-shader
                 (local grass-shader (lg.getShader))
                 (local (px py pw ph) (quad:getViewport))
                 (when (and grass-shader (grass-shader:hasUniform :spriteSize))
                   (grass-shader:send :spriteSize [pw ph])
                   (grass-shader:send :spriteAtlasPosition [px py])
                   (grass-shader:send :depth 4)
                   (grass-shader:send :highlight 0))
                 ;; (local fennel (require :lib.fennel))
                 ;; (lg.push)
                 ;; (lg.scale 0.25)
                 ;; (lg.print (fennel.view atlas.brushes.Player) 10 0)
                 ;; (lg.pop)
                 (lg.draw image quad
                          (if self.left (+ self.x 16) self.x)
                          self.y 0 (if self.left -1 1) 1)
                 (local {: atlas} (require :assets))
                 (if self.hands
                   (do (lg.push)
                       (lg.setShader)
                       (lg.draw atlas.image atlas.quads.SmallStone
                                self.x
                                (- self.y 12 (if (= self.index 1) 1 0)) )
                       (lg.pop))
                   
                   (and (> self.picked-up 0) self.picked-up-item (. atlas.quads self.picked-up-item)
                        (do
                          (lg.push)
                      (lg.setShader)
                      (lg.draw atlas.image (. atlas.quads self.picked-up-item)
                               self.x
                               (- self.y 12 (if (= self.index 1) 1 0)) )
                      (lg.pop)
                      )
                    )
                   )
                 (when  grass-shader 
                   (lg.setShader grass-shader))
                 )
               
               :draw-outline
               (fn [self image ...]
                 (local lg love.graphics)
                 (lg.setColor 1 1 1 0.05)
                 ;; (lg.setColor 1 1 1 1)
                 (local quad (match self.state
                               :idle (. idle-outline self.index)
                               :run (. run-outline self.index)))
                 (local grass-shader (lg.getShader))
                 (when (and grass-shader (grass-shader:hasUniform :spriteSize))
                   (local (px py pw ph) (quad:getViewport))
                   (grass-shader:send :spriteSize [pw ph])
                   (grass-shader:send :spriteAtlasPosition [px py]))
                 (lg.draw image quad
                          (if self.left (+ self.x 16) self.x)
                          self.y 0 (if self.left -1 1) 1))

               : draw-shadow
               ;; (fn [self image ...]
               ;;   (local lg love.graphics)
               ;;   (lg.push :all)
               ;;   (lg.setColor 1 1 1 0.3)
               ;;   (local grass-shadow (lg.getShader))
               ;;   (when grass-shadow
               ;;     (grass-shadow:send :onField false))
               ;;   (lg.draw atlas.image shadow-quad
               ;;            (+ self.x (if self.left 1 -1))
               ;;            (- self.y -14 12 ))
               ;;   (when grass-shadow
               ;;     (grass-shadow:send :onField true)
               ;;     (local {: masks} (require :assets))
               ;;     (lg.draw masks.image masks.quads.Shadow
               ;;            (+ self.x (if self.left 0 0))
               ;;            (+ self.y 3))
               ;;     (grass-shadow:send :onField false))
               ;;   (lg.pop))
               
               :update
               (fn [self dt]
                 (local isDown (fn [a b] (or (love.keyboard.isDown a)
                                             (when b (love.keyboard.isDown b)))))
                 ;; move
                 
                 (local speed (* dt 100))
                 (local vyp (+ (if (isDown :down :s) 1 0)
                              (if (isDown :up :w) (- 1) 0)))
                 (local vxp (+  (if (isDown :left :a) (- 1) 0)
                                (if (isDown :right :d) (+ 1) 0)))
                 (local mag (math.sqrt (+ (^ vyp 2) (^ vxp 2))))
                 (var vx 0)
                 (var vy 0)
                 (when (~= mag 0)
                   (set vx (* speed (/ vxp mag)))
                   (set vy (* speed (/ vyp mag))))
                 (update-direction self vx vy)
                 (local ( x y) (values (+ self.x self.encode.cx vx)
                                       (+ self.y self.encode.cy vy)))
                 (let [{: world : ui-visible} (require :state)]
                   (local (ax ay col) (world:move self x y move-filter))
                   (move-match col)
                   ;; (pp col)
                   (if ui-visible
                       (do (tset self :x (- ax self.encode.cx))
                           (tset self :y (- ay self.encode.cy)))
                       (do (tset self :x (- x self.encode.cx))
                           (tset self :y (- y self.encode.cy)))))

                 (update-state self vx vy self.state)
                 (update-animation self dt)
                 (self:set-highlight)
                 (let [state (require :state)
                       potions (require :prefab-potions)
                       goal (. potions.list state.day)
                       finished? (> (or (. self.inventory.potions goal) 0) 0)]
                   (when finished?
                     (set state.day (+ state.day 1))))
                 ;; need to give elixer to cat
                 ;; (when (> self.inventory.potions.ElixerOfLife 0)
                 ;;   (love.event.push :game-over :ElixerOfLife))
                 (when (> self.picked-up 0)
                   (tset self :picked-up (- self.picked-up dt)))
                 (set self.sx (if self.left 1 -1))
                 (when dev
                   (love.window.setTitle (.. "Player-base: " (+ self.y self.h)))
                   )
                 )
               :add-potion (fn [self potion]
                             (tset self.inventory.potions potion
                                   (+ (. self.inventory.potions potion) 1)))
               :remove-potion (fn [self potion]
                             (tset self.inventory.potions potion
                                   (- (. self.inventory.potions potion) 1)))
               :add-ingredients
               (fn [self ingredients]
                 (tset self :picked-up over-head-timing)
                 (tset self.inventory.ingredients ingredients
                       (+ (. self.inventory.ingredients ingredients) 1))
                 (tset self :picked-up-item ingredients))
               
               :remove-ingredients (fn [self ingredients]
                             (tset self.inventory.ingredients ingredients
                                   (- (. self.inventory.ingredients ingredients) 1)))
               
               :preview preview
               :handlers {}
               :draw-collider draw-collider})

(fn player.brew [self name]
  (local potions (require :prefab-potions))
  (local recipie (. potions.recipies name))
  (local ingredients self.inventory.ingredients)
  (var can-brew true)
  (var space true)
  (var reason nil)
  (set reason :already-brewed)
  (when (> (. self.inventory.potions name) 0)
    (set space false))
  ;; (pp [(. self.inventory.potions name)])
  (when space
    (set reason :missing-ingredients)
    (each [_ r (ipairs recipie)]
    (when (= (. ingredients r) 0)
      (set can-brew false)
      )))
  (when (and can-brew space)
    (set reason :success)
    (each [_ r (ipairs recipie)]
      (tset ingredients r (- (. ingredients r) 1)))
    (tset self.inventory.potions name (+ (. self.inventory.potions name) 1)))
  (values (and can-brew space) name reason))

(local player-mt {:__index player})

(local inventory {:gold 0
                  :potions {:ElixerOfLife 0
                            :MixtureOfLevitation 0
                            :CauseOfDeath 0
                            :DragonHideCompound 0
                            :BrewOfBed 0
                            :UnholyConcoction 0
                            :EssenceOfStrength 0
                            :LoveMedley 0
                            :InvisibilityPotion 0}
                  :ingredients {:Death 0
                                :Life 0
                                :Feather 0
                                :Carrot 0
                                :Seed 0 
                                :Nut 0
                                :Rosemary 0
                                :Garlic 0
                                :Lemon 0
                                :Lilly 0 
                                :Sunflower 0
                                :Dragon 0 
                                :Hemp 0
                                :Lavender 0
                                :Root 0
                                :Cherry 0}})

(fn clone-inventory [inventory]
  (local i {})
  (each [index body (pairs inventory)]
    (match (type body)
      :table 
      (do (tset i index {})
          (each [key value (pairs body)]
            (tset (. i index) key value)))
      _ (tset i index body)))
  i)

(fn player.brewable [self potion-name]
  (local potions (require :prefab-potions))
  (local ingredients self.inventory.ingredients)
  (local requirements (. potions :recipies potion-name))
  (var brewable true)
  (each [_ ing (ipairs requirements)]
    (when (= (. ingredients ing) 0)
      (set brewable false)))
  brewable)

(fn player.at-pot [self at-pot?]
  (local flux (require :lib.flux))
  (match [self.can-brew at-pot?]
    [true false] (flux.to self 0.2 {:potions-ox -20})
    [false true] (flux.to self 0.2 {:potions-ox 0}))
  (tset self :can-brew at-pot?))

(fn player.create [brush]
  (local {: add-collider} (require :prefab-object))
  (local {: atlas} (require :assets))
  (local {: px : py : pw : ph : iw : ih} brush)
   (-> {:encode brush
        :name :Player
        :quad (love.graphics.newQuad px py pw ph iw ih)
        :idle-quad [atlas.quads.Idle1 atlas.quads.Idle2]
        :idle-quad-up [atlas.quads.Idle1Up atlas.quads.Idle2Up]
        :run-quad [atlas.quads.Run1 atlas.quads.Run2]
        :run-quad-up [atlas.quads.Run1Up atlas.quads.Run2Up]
        :abilities {:strength false :cut false :levitate false}
        :index 1
        :picked-up 0
        :durration 0.5
        :durrations {:idle 0.7 :run 0.1}
        :timer 0
        :state :idle
        :unique true
        :inventory (clone-inventory inventory)
        :left false
        :right false
        :up false
        :down false
        :potions-ox -20
        :can-brew false
        :shadow true
        :shadow-quad  atlas.quads.Shadow
        :sy 2
        :sx 0
        :x brush.x :y brush.y
        :w brush.w :h brush.h}
       ((fn [x] (setmetatable x player-mt) x))
       add-collider))

(set player.ability-text (. (require :prefab-potions) :hint))

player
