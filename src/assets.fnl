(local json (require :lib.json))
(local fennel (require :lib.fennel))
(local pallet (require :pallet))

(local lg love.graphics)

(local font {})

(local atlas {})

(local masks {})

(local sounds {})

(tset sounds :bgm (love.audio.newSource "assets/music/PotionsNoBase.ogg" "static"))
(sounds.bgm:setLooping true)
(sounds.bgm:setVolume 0.6)
(tset sounds :click (love.audio.newSource "assets/sounds/click.ogg" "static"))
(tset sounds :bounce (love.audio.newSource "assets/sounds/bounce.ogg" "static"))
(tset sounds :hover (love.audio.newSource "assets/sounds/hover.ogg" "static"))
(tset sounds :page (love.audio.newSource "assets/sounds/page.ogg" "static"))
(tset sounds :meaw (love.audio.newSource "assets/sounds/meaw.ogg" "static"))
(sounds.hover:setVolume 0.02)
(sounds.bounce:setVolume 5) ;; why so quiet?

;; (set font.header (lg.newFont :assets/fonts/FreePixel.ttf 40))
;; (set font.title (lg.newFont :assets/fonts/FreePixel.ttf 50))
;; (set font.text (lg.newFont :assets/fonts/FreePixel.ttf 16))

;; (set font.main-title (lg.newFont :assets/fonts/pico-8.otf 24))
;; (set font.subtitle (lg.newFont :assets/fonts/pico-8.otf 18))

;; (set font.header (lg.newFont :assets/fonts/pico-8.otf 6))
;; (set font.title (lg.newFont :assets/fonts/pico-8.otf 12))
;; (set font.text (lg.newFont :assets/fonts/pico-8.otf 6))
;; (set font.popup (lg.newFont :assets/fonts/pico-8.otf 6))
;; (set font.description (lg.newFont :assets/fonts/pico-8.otf 6))
;; (set font.debug (lg.newFont :assets/fonts/inconsolata.otf 10))
;; (set font.inline (lg.newFont :assets/fonts/pico-8.otf 6))

(set font.main-title (lg.newFont :assets/fonts/6px-normal.ttf 24))
(set font.subtitle (lg.newFont :assets/fonts/6px-normal.ttf 16))

(set font.header (lg.newFont :assets/fonts/6px-normal.ttf 8 :mono))
(set font.title (lg.newFont :assets/fonts/6px-normal.ttf 12 :mono))
(set font.text (lg.newFont :assets/fonts/6px-normal.ttf 8 :mono))

(set font.popup (lg.newFont :assets/fonts/6px-normal.ttf 8 :mono))

(set font.debug (lg.newFont :assets/fonts/inconsolata.otf 10 :mono))

(set font.description (lg.newFont :assets/fonts/6px-normal.ttf 8 :mono))
(set font.inline (lg.newFont :assets/fonts/6px-normal.ttf 8 :mono))

(local atlas-filename "assets/sprites/atlas.png")
(local atlas-data-filename "assets/sprites/atlas.json")
(set atlas.image (lg.newImage atlas-filename))

(local masks-filename "assets/sprites/masks.png")
(local masks-data-filename "assets/sprites/masks.json")
(set masks.image (lg.newImage masks-filename))


(local ui {})
(local ui-filename "assets/sprites/UI.png")
(local ui-data-filename "assets/sprites/UI.json")
(set ui.image (lg.newImage ui-filename))

;; (pp (-> atlas-data-filename
;;          love.filesystem.read
;;          json.decode))

(fn parse-slice [slices]
  (let [ret {}]
    (each [_ s (ipairs slices)]
      (let [{: x : y : w : h} (. s :keys 1 :bounds)]
        (tset ret s.name
              {:name s.name : x : y : w : h})
        (when s.data
          (let [d (fennel.eval s.data)]
            (tset (. ret s.name) :data {})
            (each [k v (pairs d)]
              (tset (. ret s.name :data) k v))))
        ))
    ret))

(fn to-quads [slices image]
  (let [ret {}        
        (iw ih) (image:getDimensions)]
    (each [name s (pairs slices)]
      (let [{: x : y : w : h} s]
        (tset ret s.name (love.graphics.newQuad x y w h iw ih))))
    ret))

(fn to-brush [slices image]
  (let [ret {}        
        (iw ih) (image:getDimensions)]
    (each [name s (pairs slices)]
      (let [{: x : y : w : h : data} s]
        (tset ret s.name {: name
                          :px x
                          :py y
                          :pw w
                          :ph h
                          :iw iw
                          :ih ih
                          :w w
                          :h h})
        (when data
          (each [k v (pairs data)]
            (tset (. ret s.name) k v)))
        ))
    ret))

(set atlas.slices
     (-> atlas-data-filename
         love.filesystem.read
         json.decode
         (. :meta :slices)
         parse-slice
         ))

(set atlas.quads (to-quads atlas.slices atlas.image))

(set atlas.brushes (to-brush atlas.slices atlas.image))

(set ui.slices
     (-> ui-data-filename
         love.filesystem.read
         json.decode
         (. :meta :slices)
         parse-slice
         ))

(set ui.quads (to-quads ui.slices ui.image))

(set masks.slices
     (-> masks-data-filename
         love.filesystem.read
         json.decode
         (. :meta :slices)
         parse-slice
         ))
(set masks.quads (to-quads masks.slices masks.image))
(set masks.brushes (to-brush masks.slices masks.image))


(local cursor-canvas (love.graphics.newCanvas (* 11 4) (* 11 4)))
(lg.push)
(lg.setCanvas cursor-canvas)
(lg.scale 4)
(lg.draw atlas.image atlas.quads.Pointer 0 0)
(lg.setCanvas)
(lg.pop)

(local cursor-data (cursor-canvas:newImageData))

(local controls (lg.newImage :assets/sprites/Controls.png))

;; (let [clouds (require :singleton-clouds)] (clouds.init atlas))

{: font : atlas : ui : sounds : controls : cursor-data : masks}
