(local flux (require :lib.flux))
(local {: wiggles} (require :state))

(local message-delay-default 3)

(var message-delay message-delay-default)

(local message-board {})
(set message-board.flux (flux.to wiggles 0.2 {:message-y 0}))

(fn message-board.open []
  (message-board.flux:stop)
  (set message-board.flux
       (flux.to wiggles 0.2 {:message-y 62}))
  (message-board.flux:oncomplete message-board.pause))

(fn message-board.close []
  (message-board.flux:stop)
  (set message-board.flux
       (flux.to wiggles 1 {:message-y 0})))

(fn message-board.pause []
  (message-board.flux:stop)
  (set message-board.flux
       (flux.to wiggles 0.2 {:message-y 0}))
  (message-board.flux:delay message-delay)
  (message-board.flux:oncomplete message-board.close))

(var output "")
(local pallet (require :pallet))
(var fontColor pallet.shadow)
(var fontColor pallet.shadow)
(fn write [message period?]
  (set output message)
  (set message-delay (or period? message-delay-default))
  (message-board.open)
  (set fontColor pallet.shadow)
  true
  )

(fn error [message period?]
  (set output message)
  (set message-delay (or period? message-delay-default))
  (message-board.open)
  (set fontColor pallet.red)
  true
  )


(fn draw []
  (local lg love.graphics)
  (local assets (require :assets))
  (local {: ui} (require :assets))
  (lg.push)
  ;;(pp popup.y)
  (lg.translate (* 16 (- 16 13)) (- (+ (* 16 9) 4) -75 wiggles.message-y))
  (lg.draw ui.image ui.quads.Popup)
  (lg.push)
  (lg.setColor pallet.shadow)
  (lg.setFont assets.font.popup)
  (lg.printf (string.upper output) (* 4 1) 2 (* 1 (- (* 13 16) 8)) :center)
  (lg.pop)
  (lg.pop)
  )

{: write : error : draw}
