(local {: add-collider : draw-collider : preview} (require :prefab-object))

(local button {:serialize
               (fn [self]  (print (.. "Encoding " self.name)) self.encode)

               :draw
               (fn [self image ...]
                 (love.graphics.draw image (if self.down self.down-quad
                                               self.quad)
                                     self.x self.y))
               :update
               (fn [self dt]
                 ;;(pp :update)
                 (local state (require :state))
                 (local world state.world)
                 (local (l t w h) (world:getRect self))
                 (local (items len) (world:queryRect l t w h (fn [item]
                                                               (or (= item.name :Player)
                                                                   (= item.name :SmallStone)))))
                 (if (> len 0)
                     (tset self :down true)
                     (tset self :down false)))
               :handlers {}
               : draw-collider
               : preview})

(local button-mt {:__index button})

(fn button.create [brush]
  (local {: px : py : pw : ph : iw : ih : name} brush)
  (local assets (require :assets))

   (-> {:encode brush
        :name name
        :type :button
        :down false
        :quad (love.graphics.newQuad px py pw ph iw ih)
        :down-quad (. assets.atlas.quads (.. name :Down))
        :x brush.x :y brush.y
        :w brush.w :h brush.h}
       ((fn [x] (setmetatable x button-mt)))
       (add-collider :cross)
       )
  )


button
