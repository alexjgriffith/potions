(local assets (require :assets))

(fn test-level []
  (local {: editor : brush : level : creation-callbacks : preview-callbacks : ground-brushes} (require :editor-setup))
  (local l1 (level.create :l1 (* (- (* 16 4) 3) 16) (* (- (* 11 4) 3) 16)
                          {:x 16 :y 16} :assets/sprites/atlas.png
                          creation-callbacks preview-callbacks))
  (-> l1
      (l1.add-layer :ground :tile ground-brushes :assets/sprites/atlas.png)
      (l1.add-layer :buttons :objects assets.atlas.brushes)
      (l1.add-layer :objects :objects assets.atlas.brushes))
  (l1.add l1 :objects 16 16 :Player)
  (l1.add l1 :objects 16 32 :SmallStone)
  (l1.add l1 :objects (* 1 16) (* 7 16) :Stump)
  (l1.add l1 :objects (* 1 16) (* 8 16) :Stump)
  (l1.add l1 :objects (* 1 16) (* 9 16) :SmallStone)
  (l1.add l1 :objects (* 1 16) (* 5 16) :GreenFence)
  (l1.add l1 :objects (* 3 16) (* 5 16) :BlueFence)
  (l1.add l1 :objects (* 5 16) (* 5 16) :RedFence)
  (l1.add l1 :objects (* 7 16) (* 5 16) :YellowFence)
  (l1.add l1 :buttons (* 1 16) (* 3 16) :GreenButton)
  (l1.add l1 :buttons (* 3 16) (* 3 16) :BlueButton)
  (l1.add l1 :buttons (* 5 16) (* 3 16) :RedButton)
  (l1.add l1 :buttons (* 7 16) (* 3 16) :YellowButton)
  l1)

(fn empty-level [name?]
  (local {: editor : brush : level : creation-callbacks : preview-callbacks : ground-brushes} (require :editor-setup))
  (local l1 (level.create (or name? :l1) (* (- (* 16 4) 3) 16) (* (- (* 11 4) 3) 16)
                          {:x 16 :y 16} :assets/sprites/atlas.png
                          creation-callbacks preview-callbacks))
  (-> l1
      (l1.add-layer :ground :tile ground-brushes :assets/sprites/atlas.png)
      (l1.add-layer :buttons :objects assets.atlas.brushes)
      (l1.add-layer :objects :objects assets.atlas.brushes))
  (l1.add l1 :objects (* 20 16) 64 :Player)
  l1)

(tset _G :create-empty-level
      (fn [name]
        (assert name "Need to pass in a filename, like new-level")
        (local level (empty-level name))
        (level:save (.. name ".fnl"))
        ))

(fn main-level []
  (local {: editor : brush : level : creation-callbacks : preview-callbacks : ground-brushes} (require :editor-setup))
  (local {: atlas} (require :assets))
  (level.load-map-with-brushes :main-level                          
                               creation-callbacks preview-callbacks {:objects atlas.brushes :buttons atlas.brushes
                                                                     :ground ground-brushes}))

(fn load [name]
  (local {: editor : brush : level : creation-callbacks : preview-callbacks : ground-brushes} (require :editor-setup))
  (local {: atlas} (require :assets))
  (level.load-map-with-brushes name 
                  creation-callbacks preview-callbacks {:objects atlas.brushes :buttons atlas.brushes
                                                                     :ground ground-brushes}))


{: empty-level : load}
