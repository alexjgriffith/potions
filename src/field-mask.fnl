(local assets (require :assets))

(local masks {:wheat {} :water {} :cliff {}})
(set masks.wheat.canvas (love.graphics.newCanvas 1280 720))
(set masks.wheat.quads [])
(let [(iw ih) (assets.masks.image:getDimensions)
      (qx qy) (assets.masks.quads.Wheat:getViewport)]
  (for [i 1 16]
    (local quad (love.graphics.newQuad
                 (+ qx (* 16 (math.floor (% (- i 1) 4))))
                 (+ qy (* 16 (math.floor (/ (- i 1) 4))))
                 16 16 iw ih))
    (table.insert masks.wheat.quads quad))
)

;; Algo for water mask add Water, Cons and Ford
(set masks.water.canvas (love.graphics.newCanvas 1280 720))
(set masks.water.canvas-updated (love.graphics.newCanvas 1280 720))
(set masks.water.quads [])
(let [(iw ih) (assets.masks.image:getDimensions)
      (qx qy) (assets.masks.quads.Water:getViewport)]
  (for [i 1 16]
    (local quad (love.graphics.newQuad
                 (+ qx (* 16 (math.floor (% (- i 1) 4))))
                 (+ qy (* 16 (math.floor (/ (- i 1) 4))))
                 16 16 iw ih))
    (table.insert masks.water.quads quad)))

(set masks.cliff.canvas (love.graphics.newCanvas 1280 720))
(set masks.cliff.quads [])
(let [(iw ih) (assets.masks.image:getDimensions)
      (qx qy) (assets.masks.quads.Cliff:getViewport)]
  (for [i 1 16]
    (local quad (love.graphics.newQuad
                 (+ qx (* 16 (math.floor (% (- i 1) 4))))
                 (+ qy (* 16 (math.floor (/ (- i 1) 4))))
                 16 16 iw ih))
    (table.insert masks.cliff.quads quad)))


(fn structure [table]
  (each [key value (pairs table)]
    (print (.. key " <" (type value) ">") )))

;; (local wheat-brush state.level.layers.ground.brushes.wheat)

(fn concat-keys [t sep?]
  (var ret nil)
  (var sep (or sep? ", "))
  (each [key _ (ipairs t)]
    (if ret
        (set ret (.. ret sep key))
        (set ret key)))
  (or ret ""))

(fn make-mask [mask]
  (local state (require :state))
  (local layers [:buttons])
  (local ground :ground)
  ;;(pp :draw)
  (local lg love.graphics)
  (lg.push :all)
  (assert (. masks mask)
          (.. "Mask " mask " not defined. Choose from: " (concat-keys masks)))
  (lg.setCanvas (. masks mask :canvas))
  (lg.setColor 1 1 1 1)
  (lg.clear)
  ;;(lg.draw assets.masks.image)
  (each  [_ tile (ipairs (. state.level.layers ground :data))]
    (when (= tile.brush mask)
        (lg.draw assets.masks.image
                 (. (. masks mask :quads) tile.bitmap-index)
                 (* 16 (- tile.x 1))
                 (* 16 (- tile.y 1)))
        ))
  (when (= mask :water)
    (each [_ layer (ipairs layers)]
    (let [data (. state.level.layers layer :data)]
      (each [_ obj (ipairs data)]
        (when (. assets.masks.quads obj.name)
              (lg.draw assets.masks.image (. assets.masks.quads obj.name) obj.x obj.y))

        ))))
  (lg.setCanvas)
  (lg.pop)
  (. masks mask :canvas))

(local alpha-black (love.graphics.newShader :assets/shaders/alpha-black.glsl))
(fn update-mask [mask old-mask layers]
  (local state (require :state))
  (local lg love.graphics)
  (lg.push :all)
  (assert (. masks mask)
          (.. "Mask " mask " not defined. Choose from: " (concat-keys masks)))
  (lg.setCanvas (. masks mask :canvas-updated))
  (lg.setColor 1 1 1 1)
  (lg.clear)
  (lg.draw old-mask)
  
  (lg.pop)
  ;; (pp (. masks mask :canvas))
  (. masks mask :canvas-updated))

{: make-mask : update-mask}

