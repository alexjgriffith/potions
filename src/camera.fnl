(fn update-camera [player camera target]
  (match camera.type
    :game (do
            (local offset-x (if player.left -12 0))
            (local offset-y (if player.up -12 0))
            (local ix (math.floor (/ (+ player.x player.encode.cx 0 offset-x) 16)))
            (local iy (math.floor (/ (+ player.y player.encode.cx 0 offset-y) 16)))
            (local qx (lume.clamp (math.floor (/ ix 15)) 0 3))
            (local qy (lume.clamp (math.floor (/ iy 10)) 0 3))
            (local tx (- (* qx (- (* 16 16) 16))))
            (local ty (- (* qy (- (* 16 11) 16))))
            (set target.x tx)
            (set target.y ty)
            (set camera.x  ((if (> tx camera.x) math.ceil math.floor)
                            (lume.lerp camera.x target.x
                                       (if target.snap 1 0.1))))
            (set camera.y  ((if (> ty camera.y) math.ceil math.floor)
                            (lume.lerp camera.y target.y
                                       (if target.snap 1 0.1))))
            ;; (pp target)
            )
    :editor (do (set camera.x 0)
                (set camera.y 0))
    )
  (when target.snap
    (set target.snap false))
  )

{: update-camera }
