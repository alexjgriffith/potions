(var fade [:in 0])

(local (w h) (love.window.getMode))

(local range (* w 2))
(local speed 1500)

(local wipecanvas (love.graphics.newCanvas w h))

(var colour [(/ 34 256) (/ 32 256) (/ 52 256) 1])
(var message {:text "" :font nil :ellipsis "" :timer 0})


(fn set-colour [c]
  (set colour c))

(fn draw-fade [r]
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.origin)
  (love.graphics.setCanvas wipecanvas)
  (love.graphics.clear)
  (love.graphics.push)
  (love.graphics.setColor colour)
  (love.graphics.rectangle :fill 0 0 w h)
  (when (and message message.text message.font)
    (love.graphics.push)
    (love.graphics.setColor 1 1 1 1)
    (love.graphics.setFont message.font)
    (love.graphics.scale 4)
    (love.graphics.printf (.. message.text message.ellipsis)
                          0 (/ h 2 8) (/ w 4) :center)
    (love.graphics.pop))
  (love.graphics.setBlendMode :replace)
  (love.graphics.setColor 1 1 1 0)
  (love.graphics.circle :fill (/ w 2) (/ h 2) r)
  (love.graphics.setBlendMode :alpha)
  (love.graphics.pop)
  (love.graphics.setCanvas))

(var out-callback? nil)
(var in-callback? nil)
(fn fade-out [callback fade-message?]
  (set message fade-message?)
  (set out-callback? callback)
  (set fade [:out (/ w 2)]))

(fn fade-in [callback]
  (set in-callback? callback)
  (set fade [:in 0]))


(fn update [dt]
  (when (and message message.ellipsis message.timer)
    (let [l (length message.ellipsis)]
      (set message.timer (+ message.timer dt))
      (when (> message.timer 0.5)
        (set message.timer 0)
        (if (= l 3)
            (set message.ellipsis "")
            (set message.ellipsis (.. message.ellipsis "."))))))
    (match fade
    [:in x]
    (do
      (tset fade 2 (+ (* speed dt) (. fade 2)))
      (draw-fade x)
      (if (> x range)
          (set fade [:null range])
          (when in-callback?
            (in-callback?))))
    [:out x]
    (do
      (tset fade 2 (- (. fade 2) (* speed dt)))
      (draw-fade x)
      (when (< x 0)
        (set fade [:null 0])
        (when out-callback?
            (out-callback?))))
    [:null y] (do
                (love.graphics.setCanvas wipecanvas)
                (love.graphics.clear)
                (love.graphics.setCanvas))))

{: update  :out fade-out :in fade-in :canvas wipecanvas : set-colour }
