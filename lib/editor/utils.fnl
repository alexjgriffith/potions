(local once-index {})
(fn once [[which value] body]
  `(when (not (. ,once-index ,which))
     (tset ,once-index ,which ,value)
     ,body))

(local change-index {})
(fn on-change [[which value] body]
  `(when (not (= (. ,change-index ,which) ,value) )
     (tset ,change-index ,which ,value)
     body
    true))

(fn incf [value ?by]
  `(set ,value (+ ,value (or ,?by 1))))

(fn decf [value ?by]
  `(set ,value (+ ,value (or ,?by 1))))

(fn with [t keys ?body]
  `(let [,keys ,t]
     (if ,?body
         ,?body
         ,keys)))


{: on-change : once : incf : decf : with}
