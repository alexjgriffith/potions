love.conf = function(t)
   t.gammacorrect = false
   t.title, t.identity = "potions", "Potions"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 1280
   t.window.height = 720
   t.window.vsync = true
   t.window.resizable = false
   t.version = "11.4"
end
