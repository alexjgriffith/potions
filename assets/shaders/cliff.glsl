#pragma language glsl1

#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    return transform_projection * vertex_position;
}
#endif

#ifdef PIXEL
uniform vec2 spriteSize;
uniform Image mask;
uniform float scale;
uniform vec2 grassCanvasSize;
uniform vec2 pos;
uniform vec2 offset;
uniform float above;
vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
  float h = spriteSize.y;
  float w = spriteSize.x;
  
  float y = floor(texture_coords.y * (spriteSize.y + (offset.y * 2.0)) - offset.y);
  float x = floor(texture_coords.x * (spriteSize.x + (offset.x * 2.0)) - offset.x);
  vec4 maskcolor = Texel(mask,  (screen_coords + pos - offset) / grassCanvasSize);
  
  vec4 texturecolor = Texel(tex, texture_coords);
  if(maskcolor.b == 1.0 && (h - y) <= 16.0 && above != 1.0){
    texturecolor=  vec4(0.0,0.0,0.0,0.0);
    }
    // tecturecolor = maskcolor;
    return texturecolor * color;
}
#endif
