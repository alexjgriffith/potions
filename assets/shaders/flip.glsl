#pragma language glsl1

#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    return transform_projection * vertex_position;
}
#endif

#ifdef PIXEL

uniform float oh;
uniform float oy;
uniform vec2 offset;
uniform vec2 spriteSize;
vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
  float height = (2.0 * offset.y + spriteSize.y);
  vec4 texturecolor = vec4(0.0,0.0,0.0,0.0);
  if ( ((1.0 - texture_coords.y) * height ) > offset.y + (spriteSize.y - oh) ){
      texturecolor = Texel(tex,  vec2(texture_coords.x, 1.0 - (texture_coords.y * height + oy) / height ));
  }
  return texturecolor * color;
}
#endif
