#pragma language glsl1

#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    // The order of operations matters when doing matrix multiplication.
    return transform_projection * vertex_position;
}
#endif

#ifdef PIXEL
uniform Image mask;
uniform vec2 waterCanvasSize;
uniform float scale;
uniform float camera;
vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
  vec4 maskcolor = Texel(mask, screen_coords / vec2(1280.0,720.0) );
  vec4 texturecolor = Texel(tex, texture_coords);
  if (maskcolor.r == 0.0){
    texturecolor = vec4(0.0,0.0,0.0,0.0);
  }
  return texturecolor * color;
}
#endif
