#pragma language glsl1

#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    // The order of operations matters when doing matrix multiplication.
    // vpos = vertex_position;
    return transform_projection * vertex_position;
}
#endif

#ifdef PIXEL
uniform vec2 spriteSize;
uniform Image mask; // world-coordinates
uniform float scale;
uniform vec2 grassCanvasSize;
uniform vec2 camera; // shift by camera when we reduce to screen-space
uniform vec2 pos;
uniform vec2 offset;
uniform float depth;
vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
  float h = spriteSize.y;
  float w = spriteSize.x;
  float y = floor(texture_coords.y * (spriteSize.y + (offset.y * 2.0)) - offset.y);
  float x = floor(texture_coords.x * (spriteSize.x + (offset.x * 2.0)) - offset.x);
  vec4 maskcolor = Texel(mask,  (screen_coords + pos - offset) / grassCanvasSize);
  
  vec4 texturecolor = Texel(tex, texture_coords);
  const float norm = 16.0 / 256.0;
  float d = depth;
  if (y > (h - d) && maskcolor.g == 1.0 ||
      y == (h - d) && maskcolor.g >= 4.0 * norm ||
      y == (h - d + 1.0) && maskcolor.g >= 3.0 * norm ||
      y == (h - d + 2.0) && maskcolor.g >= 2.0 * norm ||
      y == (h - d + 3.0) && maskcolor.g >= 1.0 * norm){
    texturecolor=vec4(0.0,0.0,0.0,0.0);
  }
  // texturecolor=maskcolor;
  return texturecolor * color;
}
#endif
