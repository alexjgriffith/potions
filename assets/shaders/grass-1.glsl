#pragma language glsl1

#ifdef VERTEX
vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    // The order of operations matters when doing matrix multiplication.
    // vpos = vertex_position;
    return transform_projection * vertex_position;
}
#endif

#ifdef PIXEL
uniform vec2 spriteSize;
uniform vec2 atlasSize;
uniform vec2 spriteAtlasPosition;
uniform Image cliffMask;
uniform Image mask;
uniform float scale;
uniform vec2 grassCanvasSize;
uniform vec2 camera;
uniform float depth;
uniform float above;
uniform float highlight;
uniform float outline;
vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
    float quad_coords_y = (atlasSize.y / spriteSize.y) * (texture_coords.y - (spriteAtlasPosition.y / atlasSize.y));
    float quad_coords_x = (atlasSize.x / spriteSize.x) * (texture_coords.x - (spriteAtlasPosition.x / atlasSize.x));
    float h = spriteSize.y;
    float w = spriteSize.x;
    float y = floor(quad_coords_y * spriteSize.y);
    float x = floor(quad_coords_x * spriteSize.x);
    vec2 pixel = ((screen_coords / scale) - camera);
    vec4 maskcolor = Texel(mask, pixel/ grassCanvasSize);
    vec4 cliffmaskcolor = Texel(cliffMask, pixel / grassCanvasSize);
    
    vec4 texturecolor = Texel(tex, texture_coords);
    const float norm = 16.0 / 256.0;
    float d = depth; // depth
    if (y > (h - d) && maskcolor.g == 1.0 ||
        y == (h - d) && maskcolor.g >= 4.0 * norm ||
        y == (h - d + 1.0) && maskcolor.g >= 3.0 * norm ||
        y == (h - d + 2.0) && maskcolor.g >= 2.0 * norm ||
        y == (h - d + 3.0) && maskcolor.g >= 1.0 * norm){
      texturecolor=vec4(0.0,0.0,0.0,0.0);
    }

    if (texturecolor.r == 0.0 &&
        texturecolor.g == 0.0 &&
        texturecolor.b == 0.0 &&
        texturecolor.a == 1.0 &&
        highlight == 2.0){
      texturecolor= texturecolor + vec4(1.0,1.0,1.0,1.0);
    }
    
    if (outline == 1.0){
      if ((texturecolor.r != 0.0 &&
           texturecolor.g != 0.0 &&
           texturecolor.b != 0.0) ||
          texturecolor.a == 0.0
          ){
        texturecolor=  vec4(0.0,0.0,0.0,0.0);
      }else{
        texturecolor=  vec4(0.0,0.0,0.0,0.2);
      }}

    if(cliffmaskcolor.b == 1.0 && (h - y) <= 16.0 && above != 1.0){
      texturecolor=  vec4(0.0,0.0,0.0,0.0);
    }
    return texturecolor * color;
}
#endif
