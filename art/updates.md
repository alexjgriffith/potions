## Post Jam Updates

## DONE
- New background tune
- Added Mute Button
- Enable Game Replay
- Greatly reduced loading time
- Interactable objects are highlighted more consistently
- Add outline to rocks / other movable items
- Add mask for the tops of cliff for sprites and shadows
- Changed font from Pico-8 to 6px-normal (4base to 6 base)

### 2023-03-10
- Add shadows
- Add grass shader
- Highlight interactable objects
- Make sound when you fail to interact with something
- Send an err with the potion you need to interact with something
- Add blocking wizard who needs love medley
- Make ingredients stand out more
- Manually pick up items
- Add cat mcguffain, need the elixir of life to save your cat
- Animate pot
- Add text box for opening and wizard details
- Force you to be at pot to make potions
- Add hover to objective that gives the user a hint
- Have inventory rather than fixed available products

### TODO
- Particles
- Add merchant who buys the unholy concoction
- Add fire (dragonhidecompound)
- Add warp to pot (brew of bed)
- Add blocking character for potion of invisibility
- WaterFX
- TreeFx
- Pickup cat, deal with bug where multiple stones can be dropped at the same location
