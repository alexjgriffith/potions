
* Profile
+-----+-------------+---------+----------+----------------------+ 
 | #   | Function    | Calls   | Time     | Code                 | 
 +-----+-------------+---------+----------+----------------------+ 
 | 6   | deserialize | 1       | 1.236384 | map.fnl:134          | 
 | 7   | decode      | 1       | 1.223062 | lib/autotile.fnl:240 | 
 | 8   | neighbours  | 5       | 0.901374 | lib/autotile.fnl:163 | 
 | 9   | f_n         | 12505   | 0.850678 | lib/autotile.fnl:112 | 
 | 10  | n           | 12635   | 0.580665 | lib/autotile.fnl:95  | 
 | 13  | check       | 101080  | 0.370517 | lib/autotile.fnl:96  | 
 | 16  | match-key   | 5       | 0.165216 | lib/autotile.fnl:169 | 
 | 20  | to-sparse   | 5       | 0.080397 | lib/autotile.fnl:195 | 
 | 24  | set-index   | 12505   | 0.044668 | lib/autotile.fnl:3   | 
 | 29  | parse       | 5       | 0.028442 | lib/autotile.fnl:153 | 
 | 30  | match-bitmap| 5       | 0.02586  | lib/autotile.fnl:187 | 

* Analysis
See why these take so long
check
f_n
n

neighbors -> f_n -> n -> check

we are performing 101,080 index checks. This could be halved but really we
just need to speed up this operation

;; we could cache this on map save?
#+BEGIN_SRC fennel
(fn n [t i w d]
  (fn check [t i oy ox d]
    (local row (math.floor (/ (+ i oy -1) w)))
    (local index (+ i oy ox))
    (local index-row (math.floor (/ (- index 1) w)))
    (if (and (= index-row row) (. t index))
        (. t index)
        d))
  [(check t i (- w) -1 d) ;; tl
   (check t i (- w) 0  d) ;; t
   (check t i (- w) 1  d) ;; tr
   (check t i 0 1 d)      ;; r
   (check t i w 1 d)      ;; br
   (check t i w 0  d)     ;; b
   (check t i w -1 d)     ;; bl
   (check t i 0 -1 d)])   ;; l

(fn f-n [t i w d]  
  (let [r [(. t i)]
        t2 (n t i w d)]
    (each [k v (ipairs t2)]
      (table.insert r  v))
    r))

(fn autotile.neighbours [map w]
  (let [t []]
    (each [i value (ipairs map)]
      (table.insert t (f-n map i w "_")))
    t))
#+END_SRC

* Proposed Solution
Try a better optimized loop, posibly n(log n) rather than n
transformation
[AAA]->[[tl t tr r br b bl l] [tl t tr r br b bl l] [tl t tr r br b bl l]]
(autotile.neil)
#+BEGIN_SRC fennel

#+END_SRC


*** Round 2 LUAJIT
 +-----+-------------------------------+-------------+--------------------------+----------------------------------+ 
 | #   | Function                      | Calls       | Time                     | Code                             | 
 +-----+-------------------------------+-------------+--------------------------+----------------------------------+ 
 | 8   | deserialize                   | 1           | 0.416692                 | /potions//lib/editor/map.fnl:134 |  
 | 9   | decode                        | 1           | 0.407084                 | /lib/editor/lib/autotile.fnl:280 | 
 | 12  | match-key                     | 5           | 0.176327                 | /lib/editor/lib/autotile.fnl:209 | 
 | 16  | to-sparse                     | 5           | 0.068319                 | /lib/editor/lib/autotile.fnl:235 | 
 | 17  | neighbours                    | 5           | 0.058521999999999        | /lib/editor/lib/autotile.fnl:163 | 
 | 21  | parse                         | 5           | 0.047522999999999        | /lib/editor/lib/autotile.fnl:153 | 
 | 24  | set-index                     | 12505       | 0.03776500000002         | s//lib/editor/lib/autotile.fnl:3 | 
 | 27  | match-bitmap                  | 5           | 0.028767000000001        | /lib/editor/lib/autotile.fnl:227 | 
 | 28  | parse-bitmap                  | 10          | 0.028589                 | /lib/editor/lib/autotile.fnl:119 | 
 | 31  | set-quads                     | 3           | 0.019659                 | otions//lib/editor/level.fnl:168 | 
 | 32  | quad-regions                  | 1           | 0.019535                 | /potions//lib/editor/map.fnl:240 | 
 | 35  | _23_                          | 12505       | 0.017601000000022        | /lib/editor/lib/autotile.fnl:199 | 
 | 38  | merge                         | 4           | 0.016726                 | /lib/editor/lib/autotile.fnl:249 | 
 | 43  | deserialize_objects           | 2           | 0.0077929999999995       | /potions//lib/editor/map.fnl:119 | 
 | 44  | increment_r                   | 608         | 0.007019000000013        | /lib/editor/lib/autotile.fnl:135 | 
 | 45  | n                             | 130         | 0.0067549999999956       | //lib/editor/lib/autotile.fnl:95 | 
 | 47  | add-collider                  | 181         | 0.0055630000000031       | otions//src/prefab-object.fnl:76 | 
 | 48  | ?                             | 137         | 0.0055040000000011       | tions//src/prefab-object.fnl:106 | 
 | 49  | add                           | 181         | 0.0049310000000018       | lib/bump.lua:616                 | 
 | 50  | check                         | 1040        | 0.004454                 | //lib/editor/lib/autotile.fnl:96 | 
 | 51  | eval_opts                     | 1           | 0.004113                 | lib/fennel.lua:5041              | 
 | 52  | current-global-names          | 1           | 0.004077                 | lib/fennel.lua:718               | 
 | 53  | kvmap                         | 1           | 0.004054                 | lib/fennel.lua:4669              | 
 | 54  | add-spritebatch               | 1           | 0.0040240000000002       | otions//lib/editor/level.fnl:209 | 
 | 55  | update-spritebatch            | 1           | 0.0039939999999996       | otions//lib/editor/level.fnl:181 | 
 | 56  | peephole                      | 183         | 0.0034669999999988       | lib/fennel.lua:4649              | 
 | 57  | assertIsRect                  | 181         | 0.0023680000000006       | lib/bump.lua:60                  | 
 | 58  | expand_r                      | 208         | 0.0021909999999989       | /lib/editor/lib/autotile.fnl:138 | 
 | 59  | set-brushes                   | 1           | 0.0017849999999999       | /lib/editor/lib/autotile.fnl:244 | 
 | 60  | remove                        | 1039        | 0.00176400000001         | [builtin:remove]:-1              | 
 | 61  | quad-regions                  | 5           | 0.0012390000000018       | /lib/editor/lib/autotile.fnl:256 | 
 | 62  | grid_toCellRect               | 181         | 0.0011619999999981       | lib/bump.lua:259                 | 
 | 63  | load-code                     | 1           | 0.00087700000000002      | lib/fennel.lua:740               | 
 | 64  | assertIsPositiveNumber        | 362         | 0.00064899999999746      | lib/bump.lua:54                  | 
 | 65  | assertType                    | 362         | 0.00064700000000339      | lib/bump.lua:48                  | 
 | 66  | string-stream                 | 1           | 0.00060700000000002      | lib/fennel.lua:3488              | 
 | 67  | grid_toCell                   | 181         | 0.00045699999999904      | lib/bump.lua:210                 | 
 | 68  | addItemToCell                 | 205         | 0.00041299999999822      | lib/bump.lua:342                 | 
 | 69  | _31_                          | 137         | 0.00031499999999784      | tions//src/prefab-object.fnl:123 | 
 | 70  | rebrush-encoded-objects       | 1           | 0.00024599999999975      | /potions//lib/editor/map.fnl:155 | 
 | 71  | f0                            | 59          | 0.00011200000000022      | lib/fennel.lua:2254              | 
 | 72  | quad_regions_to_quads         | 1           | 9.9000000000515e-05      | otions//lib/editor/level.fnl:160 | 
 | 73  | compile-string                | 1           | 9.1999999999537e-05      | lib/fennel.lua:2508              | 
 | 74  | ?                             | 1           | 7.1000000000043e-05      | lib/fennel.lua:1901              | 
 | 75  | ?                             | 40          | 6.199999999934e-05       | lib/fennel.lua:3751              | 
 | 76  | flatten                       | 2           | 5.3999999999554e-05      | lib/fennel.lua:2437              | 
 | 77  | search_module                 | 1           | 4.8999999999966e-05      | lib/fennel.lua:1868              | 
 | 78  | try_path                      | 2           | 3.2000000000032e-05      | lib/fennel.lua:1855              | 
 | 79  | copy                          | 4           | 2.4999999999942e-05      | lib/fennel.lua:4693              | 
 | 80  | ast-source                    | 1           | 1.70000000006e-05        | lib/fennel.lua:4924              | 
 | 81  | table_3f                      | 1           | 1.3000000000041e-05      | lib/fennel.lua:4888              | 
 | 82  | _14_                          | 5           | 1.0999999999761e-05      | potions//src/prefab-fence.fnl:52 | 
 | 83  | make_scope                    | 1           | 9.9999999999545e-06      | lib/fennel.lua:2186              | 
 | 84  | hook                          | 1           | 9.0000000003698e-06      | lib/fennel.lua:4986              | 
 | 85  | clone_inventory               | 1           | 9.0000000003698e-06      | tions//src/prefab-player.fnl:378 | 
 | 86  | make_autotile_params          | 1           | 8.00000000023e-06        | /potions//lib/editor/map.fnl:126 | 
 | 87  | f0                            | 1           | 5.9999999999505e-06      | lib/fennel.lua:2419              | 
 | 88  | _708_                         | 1           | 4.0000000005591e-06      | lib/fennel.lua:5059              | 
 | 89  | _542_                         | 1           | 2.9999999999752e-06      | lib/fennel.lua:1878              | 
 | 90  | parser                        | 1           | 2.9999999999752e-06      | lib/fennel.lua:3515              | 
 | 91  | get-layer                     | 2           | 2.0000000002796e-06      | /potions//lib/editor/map.fnl:224 | 
 | 92  | _97_                          | 1           | 2.0000000002796e-06      | tions//src/prefab-player.fnl:434 | 
 | 93  | keep_side_effects             | 1           | 2.0000000002796e-06      | lib/fennel.lua:2553              | 
 | 94  | set-reset                     | 1           | 1.9999999999465e-06      | lib/fennel.lua:4963              | 
 | 95  | _170_                         | 1           | 1.0000000001398e-06      | lib/fennel.lua:4995              | 
 | 96  | reset                         | 1           | 1.0000000001398e-06      | lib/fennel.lua:4969              | 
 | 97  | eval_env                      | 1           | 1.0000000000288e-06      | lib/fennel.lua:5030              | 
 | 98  | _167_                         | 1           | 9.999999992516e-07       | lib/fennel.lua:4988              | 
 +-----+-------------------------------+-------------+--------------------------+----------------------------------+ 

*** LUA
 +-----+-------------------------------+-------------+--------------------------+----------------------------------+ 
| #   | Function                      | Calls       | Time                     | Code                             | 
+-----+-------------------------------+-------------+--------------------------+----------------------------------+ 
| 1   | deserialize                   | 1           | 3.997                    | [string "(local map {})..."]:156 | 
| 2   | decode                        | 1           | 3.956                    | ng "(local autotile {})..."]:262 | 
| 3   | ?                             | 100088      | 2.181                    | lib/fennel.lua:688               | 
| 4   | match-key                     | 5           | 1.295                    | ng "(local autotile {})..."]:184 | 
| 5   | parse                         | 5           | 0.734                    | ng "(local autotile {})..."]:107 | 
| 6   | global-unmangling             | 100088      | 0.687                    | lib/fennel.lua:2254              | 
| 7   | string?                       | 100088      | 0.68500000000001         | lib/fennel.lua:4891              | 
| 8   | neighbours                    | 5           | 0.656                    | ng "(local autotile {})..."]:122 | 
| 9   | to-sparse                     | 5           | 0.654                    | ng "(local autotile {})..."]:222 | 
| 10  | set-index                     | 12505       | 0.473                    | ring "(local autotile {})..."]:2 | 
| 11  | match-bitmap                  | 5           | 0.422                    | ng "(local autotile {})..."]:214 | 
| 12  | main_level                    | 3           | 0.323                    | tring "(local level {})..."]:276 | 
| 13  | parse-bitmap                  | 10          | 0.298                    | ing "(local autotile {})..."]:53 | 
| 14  | set-quads                     | 3           | 0.202                    | tring "(local level {})..."]:208 | 
| 15  | quad-regions                  | 1           | 0.2                      | [string "(local map {})..."]:305 | 
| 16  | _23_                          | 12505       | 0.087999999999997        | ng "(local autotile {})..."]:172 | 
| 17  | n                             | 130         | 0.071000000000001        | ing "(local autotile {})..."]:32 | 
| 18  | check                         | 1040        | 0.066000000000001        | ing "(local autotile {})..."]:33 | 
| 19  | quad-regions                  | 5           | 0.052                    | ng "(local autotile {})..."]:246 | 
| 20  | merge                         | 4           | 0.045                    | ng "(local autotile {})..."]:235 | 
| 21  | increment_r                   | 608         | 0.032999999999998        | ing "(local autotile {})..."]:76 | 
| 22  | deserialize_objects           | 2           | 0.031                    | [string "(local map {})..."]:142 | 
| 23  | expand_r                      | 208         | 0.03                     | ing "(local autotile {})..."]:82 | 
| 24  | add-spritebatch               | 1           | 0.021999999999999        | tring "(local level {})..."]:265 | 
| 25  | update-spritebatch            | 1           | 0.021                    | tring "(local level {})..."]:226 | 
| 26  | ?                             | 137         | 0.02                     | prefab-object.lua:106            | 
| 27  | rebrush-encoded-objects       | 1           | 0.02                     | [string "(local map {})..."]:178 | 
| 28  | ?                             | 225         | 0.019                    | prefab-object.lua:76             | 
| 29  | add                           | 181         | 0.015                    | lib/bump.lua:616                 | 
| 30  | set-brushes                   | 1           | 0.0099999999999998       | ng "(local autotile {})..."]:229 | 
| 31  | assertIsRect                  | 181         | 0.0070000000000003       | lib/bump.lua:60                  | 
| 32  | grid_toCellRect               | 181         | 0.004                    | lib/bump.lua:259                 | 
| 33  | assertIsPositiveNumber        | 362         | 0.003                    | lib/bump.lua:54                  | 
| 34  | addItemToCell                 | 205         | 0.003                    | lib/bump.lua:342                 | 
| 35  | grid_toCell                   | 181         | 0.002                    | lib/bump.lua:210                 | 
| 36  | assertType                    | 362         | 0.002                    | lib/bump.lua:48                  | 
| 37  | quad_regions_to_quads         | 1           | 0.0019999999999998       | tring "(local level {})..."]:191 | 
| 38  | _31_                          | 137         | 0.001                    | prefab-object.lua:123            | 
| 39  | _17_                          | 27          | 0.001                    | prefab-ingredients.lua:135       | 
| 40  | _14_                          | 5           | 0                        | prefab-fence.lua:52              | 
| 41  | _10_                          | 4           | 0                        | prefab-button.lua:41             | 
| 42  | _9_                           | 3           | 0                        | prefab-ford.lua:34               | 
| 43  | get-layer                     | 2           | 0                        | [string "(local map {})..."]:288 | 
| 44  | _10_                          | 2           | 0                        | prefab-pot.lua:69                | 
| 45  | _28_                          | 1           | 0                        | prefab-wizard.lua:157            | 
| 46  | clone_inventory               | 1           | 0                        | prefab-player.lua:378            | 
| 47  | _97_                          | 1           | 0                        | prefab-player.lua:434            | 
| 48  | _12_                          | 1           | 0                        | prefab-cat.lua:69                | 
| 49  | make_autotile_params          | 1           | 0                        | [string "(local map {})..."]:149 | 
+-----+-------------------------------+-------------+--------------------------+----------------------------------+ 

*** LUA After math-key-fast
 +-----+-------------------------------+-------------+--------------------------+----------------------------------+ 
  | #   | Function                      | Calls       | Time                     | Code                             | 
  +-----+-------------------------------+-------------+--------------------------+----------------------------------+ 
  | 1   | deserialize                   | 1           | 4.624                    | [string "(local map {})..."]:156 | 
  | 2   | decode                        | 1           | 4.55                     | ng "(local autotile {})..."]:316 | 
  | 3   | ?                             | 75073       | 2.859                    | lib/fennel.lua:688               | 
  | 4   | to-sparse                     | 5           | 1.535                    | ng "(local autotile {})..."]:276 | 
  | 5   | parse                         | 5           | 1.275                    | ng "(local autotile {})..."]:107 | 
  | 6   | set-index                     | 12505       | 1.099                    | ring "(local autotile {})..."]:2 | 
  | 7   | global-unmangling             | 75073       | 0.92500000000001         | lib/fennel.lua:2254              | 
  | 8   | string?                       | 75073       | 0.89400000000001         | lib/fennel.lua:4891              | 
  | 9   | match-bitmap                  | 5           | 0.727                    | ng "(local autotile {})..."]:268 | 
  | 10  | match-key-fast                | 5           | 0.643                    | ng "(local autotile {})..."]:184 | 
  | 11  | parse-bitmap                  | 10          | 0.587                    | ing "(local autotile {})..."]:53 | 
  | 12  | main_level                    | 3           | 0.567                    | tring "(local level {})..."]:276 | 
  | 13  | set-quads                     | 3           | 0.357                    | tring "(local level {})..."]:208 | 
  | 14  | quad-regions                  | 1           | 0.352                    | [string "(local map {})..."]:305 | 
  | 15  | n                             | 130         | 0.18                     | ing "(local autotile {})..."]:32 | 
  | 16  | check                         | 1040        | 0.142                    | ing "(local autotile {})..."]:33 | 
  | 17  | merge                         | 4           | 0.077                    | ng "(local autotile {})..."]:289 | 
  | 18  | expand_r                      | 208         | 0.060000000000001        | ing "(local autotile {})..."]:82 | 
  | 19  | increment_r                   | 608         | 0.058999999999999        | ing "(local autotile {})..."]:76 | 
  | 20  | deserialize_objects           | 2           | 0.057                    | [string "(local map {})..."]:142 | 
  | 21  | quad-regions                  | 5           | 0.053                    | ng "(local autotile {})..."]:300 | 
  | 22  | add-spritebatch               | 1           | 0.044                    | tring "(local level {})..."]:265 | 
  | 23  | update-spritebatch            | 1           | 0.043                    | tring "(local level {})..."]:226 | 
  | 24  | ?                             | 225         | 0.037                    | prefab-object.lua:76             | 
  | 25  | ?                             | 137         | 0.037                    | prefab-object.lua:106            | 
  | 26  | add                           | 181         | 0.033                    | lib/bump.lua:616                 | 
  | 27  | rebrush-encoded-objects       | 1           | 0.027                    | [string "(local map {})..."]:178 | 
  | 28  | set-brushes                   | 1           | 0.016                    | ng "(local autotile {})..."]:283 | 
  | 29  | assertIsRect                  | 181         | 0.015                    | lib/bump.lua:60                  | 
  | 30  | grid_toCellRect               | 181         | 0.013                    | lib/bump.lua:259                 | 
  | 31  | assertIsPositiveNumber        | 362         | 0.0070000000000003       | lib/bump.lua:54                  | 
  | 32  | grid_toCell                   | 181         | 0.006                    | lib/bump.lua:210                 | 
  | 33  | quad_regions_to_quads         | 1           | 0.0049999999999999       | tring "(local level {})..."]:191 | 
  | 34  | assertType                    | 362         | 0.002                    | lib/bump.lua:48                  | 
  | 35  | addItemToCell                 | 205         | 0.001                    | lib/bump.lua:342                 | 
  | 36  | _31_                          | 137         | 0.001                    | prefab-object.lua:123            | 
  | 37  | _17_                          | 27          | 0                        | prefab-ingredients.lua:135       | 
  | 38  | _14_                          | 5           | 0                        | prefab-fence.lua:52              | 
  | 39  | _10_                          | 4           | 0                        | prefab-button.lua:41             | 
  | 40  | _9_                           | 3           | 0                        | prefab-ford.lua:34               | 
  | 41  | _10_                          | 2           | 0                        | prefab-pot.lua:69                | 
  | 42  | get-layer                     | 2           | 0                        | [string "(local map {})..."]:288 | 
  | 43  | make_autotile_params          | 1           | 0                        | [string "(local map {})..."]:149 | 
  | 44  | _28_                          | 1           | 0                        | prefab-wizard.lua:157            | 
  | 45  | clone_inventory               | 1           | 0                        | prefab-player.lua:378            | 
  | 46  | _97_                          | 1           | 0                        | prefab-player.lua:434            | 
  | 47  | _12_                          | 1           | 0                        | prefab-cat.lua:69                | 
  +-----+-------------------------------+-------------+--------------------------+----------------------------------+ 
   
