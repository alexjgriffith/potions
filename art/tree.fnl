(fn _tree [t]
  (local tr [])
  ;; iterate all tables
  (fn table-type [table]
    (local (separator opener closer) (values " " "[" "]"))
      (var str opener)
      (each [key value (pairs table)]
        (set str (.. str key separator)))
      (.. (str:sub 1 (- (# str) 1)) closer))
  (each [key value (pairs t)]
    (match (type value)
      :table (let [existing (. tr key)]
               (if existing
                   (tset (. tr key) :count (+ 1 existing.count))
                   (tset tr key {: key :type (table-type value) :count 1 :value (tree value)})))
      _ (tset tr key {: key :type (type value) :count 1 : value})))
  tr)

(local sample-table
       {:test [1 2 3 4 5 6 {:a [1 2 3] :b {:a 1 :b 2} :c true} [1 2 3]]
        :best true
        :rest {:a 1 :b 2 :c 3}
        :test2 [{:a 1 :b 2 :c 3}
                {:e 1 :f 2 :g 3}]
        :chest {:a [{:a 1 :b 2 :c 3}
                    {:a 1 :b 2 :c 3}
                    {:a 1 :b 2 :c 3}]
                :b [{:a 1 :b 2 :c 3}
                    {:a 1 :b 2 :c 3}
                    {:a 1 :b 2 :c 3}]
                :c [{:a 1 :b 2 :c 3}
                    {:a 1 :b 2 :c 3}
                    {:a 1 :b 2 :c 3}]}})

;; named vs unnamed (ignore mixed tables for now)
;; array, table, other
;; assume arrays are always of the same type?
;; test [number 6 table 1]
;; . [a b c] table
;; .. a
;; best bool
;; test2 [array 2]
;; . [a b c] (1)
;; .. a number
;; .. b number
;; .. c number
;; . [d e f (1)
;; .. d number
;; .. e number
;; .. f number
;; rest [a b c] (1)
;; . a number 1
;; . b number 2
;; . c number 3
;; test2 []
;; . 
;; chest [a b c] (1)
;; . a [a b c] (3)
;; .. a number (1)
;; .. b number (1)
;; .. c number (1) 
;; . b [a b c] (3)
;; . c [a b c] (3)

(fn tree [t]
  (fn is-array [t] (and (= :table (type t)) (. t 1)))
  ;; are you table?
  ;; no return [key type]
  ;; yes keep going down
  (fn table-names [table]
    (local (separator opener closer) (values " " "[" "]"))
      (var str opener)
      (each [key value (pairs table)]
        (set str (.. str key separator)))
      (.. (str:sub 1 (- (# str) 1)) closer))
  (fn table-types [table]
    (local (separator opener closer) (values " " "[" "]"))
      (var str opener)
      (each [key value (pairs table)]
        (set str (.. str (type value) separator)))
      (.. (str:sub 1 (- (# str) 1)) closer))
  (var ret [])
  (fn process [key array]
    [key (table-types array)])
  (each [key e (pairs t)]
    (if (= (type e) :table)
        (process key (tree e))
        (table.insert ret [key (type e)])))
  ret
  )
